package com.wipro.app.msr.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.wipro.app.msr.controller.ControllerFacade;
import com.wipro.app.msr.controller.resource.Resource;
import com.wipro.app.msr.generic.web.BaseFacesBean;
import com.wipro.app.msr.user.User;
import com.wipro.app.msr.util.PasswordEncoderGenerator;
import com.wipro.app.msr.util.constants.Messages;
import com.wipro.app.msr.util.constants.UserStatusEnum;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller
@Scope("session")
@ManagedBean(name = "userFacesBean")
public class UserFacesBean extends BaseFacesBean<User> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2180485044325012093L;
	
	@Autowired
	private ControllerFacade facade;	
	
	private List<SelectItem> resources;
	
	private List<SelectItem> status;
	
	private List<SelectItem> roles;
	
	private String confirmPassword;
	
	public UserFacesBean() {
		super("user");
		item = new User();
		item.setStatus(true);
	}
	
	public void clean() {
		try {
			list = facade.getUserController().selectAll();
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<SelectItem> listStatus() {
		this.status = new ArrayList<SelectItem>();
		this.status.add(new SelectItem(true, UserStatusEnum.ACTIVE.getName()));
		this.status.add(new SelectItem(false, UserStatusEnum.INACTIVE.getName()));		
		return this.status;
	}
	
	public List<SelectItem> listRoles() {
		this.roles = new ArrayList<SelectItem>();		
		this.roles.add(new SelectItem("SUPER_USER", "Super User"));
		this.roles.add(new SelectItem("REVIEWER", "Reviewer"));
		this.roles.add(new SelectItem("USER", "User"));
		this.roles.add(new SelectItem("MANAGER", "Manager"));
		return this.roles;
	}
	
	
	public List<SelectItem> listResources() {
		try {
			this.resources = new ArrayList<SelectItem>();
			this.resources.add(new SelectItem(null,"..."));
			List<Resource> resourceList = facade.getResourceController().selectAll();
			for (Resource resource : resourceList) {
				this.resources.add(new SelectItem(resource, resource.getName()));
			}
			
			return this.resources;
			
		} catch (ControllerException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public String save() {		
			try {
				if (item.getId() == null) {
					item.setPassword(PasswordEncoderGenerator.encodePassword(item.getPassword()));
					facade.getUserController().insert(item);
				} else {					
					facade.getUserController().update(item);
				}
				addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_SUCCESS, "User");
				item = new User();
				clean();
				return getListPath();
			} catch (ControllerException e) {
				addWebMessage(e);
			}
		
		return null;
	}
	
	public String add() {
		action = "Add";
		item = new User();
		return getFormPath();
	}

	public String edit(User row) {
		action = "Edit";
		item = row;
		return getFormPath();
	}

	public String cancel() {
		action = "";
		item = new User();
		return getListPath();
	}
	
	public Converter getResourceConverter() {
		return getEntityConverter(Resource.class, "resource");
	}

	public void delete(User row) {		
		try {				
			facade.getUserController().delete(row.getId());				
			addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_REMOVE_SUCCESS, "User");	
			clean();
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		
	}
	
	public void resetPassword(User row) {
		
		try {
			row.setPassword(PasswordEncoderGenerator.encodePassword("123456"));
			facade.getUserController().update(row);			
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,"","Password reseted.");
			FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			
		} catch (ControllerException e) {			
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Reset Password failed");
			FacesContext.getCurrentInstance().addMessage(null, facesMsg);
		}
		
	}
	
	
}

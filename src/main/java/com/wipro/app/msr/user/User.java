package com.wipro.app.msr.user;

import java.util.Objects;

import com.wipro.app.msr.controller.resource.Resource;
import com.wipro.app.msr.util.interfaces.IPrimaryKey;

public class User implements IPrimaryKey<Integer> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4204000675326414273L;
	
	private Integer id;
	private String login;
	private String password;
	private String email;
	private Boolean status;
	private String role;
	private Resource resource;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Resource getResource() {
		return resource;
	}
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final User other = (User) obj;
		return Objects.equals(this.id, other.id);
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", login=" + login + ", email=" + email + ","
				+ ", role=" + role + ", resource=" + resource + ", status=" + status + "]";
	}

	@Override
	public Integer getPrimaryKey() {
		return id;
	}
	
	
	
	
	
	
	
	
	

}

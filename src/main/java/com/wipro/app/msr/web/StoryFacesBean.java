package com.wipro.app.msr.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.wipro.app.msr.alm.ALMFilter;
import com.wipro.app.msr.alm.ALMStory;
import com.wipro.app.msr.alm.FilterTypeEnum;
import com.wipro.app.msr.controller.ControllerFacade;
import com.wipro.app.msr.controller.businessleader.BusinessLeader;
import com.wipro.app.msr.controller.resource.Resource;
import com.wipro.app.msr.controller.story.Story;
import com.wipro.app.msr.generic.web.BaseFacesBean;
import com.wipro.app.msr.job.StoryJob;
import com.wipro.app.msr.user.User;
import com.wipro.app.msr.util.constants.Messages;
import com.wipro.app.msr.util.constants.RoleEnum;
import com.wipro.app.msr.util.constants.TicketStatusEnum;
import com.wipro.app.msr.util.constants.UserStatusEnum;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller
@Scope("session")
@ManagedBean(name = "storyFacesBean")
public class StoryFacesBean extends BaseFacesBean<Story> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ControllerFacade facade;
	
	private List<SelectItem> status;
	private List<SelectItem> businessLeaders;
	private List<SelectItem> resources;
	private List<SelectItem> roles;
	private Date startDate;
	private Date endDate;
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private User loggedUser;
	private List<ALMStory> almStories;
	private ALMFilter almFilter;
	private List<SelectItem> almFilterTypeList;

	public StoryFacesBean() {
		super("story");
		item = new Story();
		filter = new Story();
		setDefaultDates();		
		almStories = new ArrayList<ALMStory>();
		almFilter = new ALMFilter();
	}
	
	private User getUser() {
		if(loggedUser == null){		
			String userName = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
			try {
				loggedUser = facade.getUserController().getUserByLogin(userName);
			} catch (ControllerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return loggedUser;

	}

	private void setResourceDefault() {
		Resource resource = getUser().getResource();
		if (resource != null) {
			item.setResource(resource);
		}

	}

	private void setDefaultDates() {
		Calendar calendar = GregorianCalendar.getInstance();
		int month = calendar.get(Calendar.MONTH);
		Calendar calendarStartDate = GregorianCalendar.getInstance();
		calendarStartDate.set(Calendar.MONTH, month - 1);
		calendarStartDate.set(Calendar.DAY_OF_MONTH, 25);
		startDate = calendarStartDate.getTime();
		Calendar calendarEndDate = GregorianCalendar.getInstance();
		calendarEndDate.set(Calendar.DAY_OF_MONTH, 25);
		endDate = calendarEndDate.getTime();

	}

	public void filter() {
		try {
			if (filter.getNumber() != null) {
				if (filter.getNumber().isEmpty()) {
					filter.setNumber(null);
				} else {
					filter.setNumber(filter.getNumber().trim());
				}
			}
			if (filter.getRole() == RoleEnum.NONE) {
				filter.setRole(null);
			}
			if (filter.getStatus() == TicketStatusEnum.NONE) {
				filter.setStatus(null);
			}
			filter.setStartDate(simpleDateFormat.format(startDate));
			filter.setEndDate(simpleDateFormat.format(endDate));
			setDefaultUserFilter();
			list = facade.getStoryController().selectBy(filter);
		} catch (ControllerException e) {
			e.printStackTrace();
		}
	}

	public void clean() {
		filter = new Story();
		setDefaultDates();
		filter();
	}
	
	public void cleanAlmStories(){
		almStories = new ArrayList<ALMStory>();
		almFilter = new ALMFilter(); 
		
	}
	
	public void filterAlmStories() {
		try {
			almStories = facade.getStoryController().getALMStories(almFilter);
			if(getLoggedUser() != null){
				for (ALMStory almStory : almStories) {
					almStory.setBusinessLeader(getLoggedUser().getResource().getBusinessLeader());
					almStory.setRole(getLoggedUser().getResource().getRole());
					almStory.setResource(getLoggedUser().getResource());
				}
			}
		} catch (ControllerException e) {
			e.printStackTrace();
		}
	}	

	public List<SelectItem> listStatus() {
		this.status = new ArrayList<SelectItem>();
		for (TicketStatusEnum status : TicketStatusEnum.values()) {
			this.status.add(new SelectItem(status, status.toString()));
		}
		return this.status;
	}

	public List<SelectItem> listRoles() {
		this.roles = new ArrayList<SelectItem>();
		for (RoleEnum role : RoleEnum.values()) {
			this.roles.add(new SelectItem(role, role.toString()));
		}
		return this.roles;
	}
	
	public List<SelectItem> listAlmFilterType() {
		this.almFilterTypeList = new ArrayList<SelectItem>();
		for (FilterTypeEnum filter : FilterTypeEnum.values()) {
			this.almFilterTypeList.add(new SelectItem(filter, filter.toString()));
		}
		return this.almFilterTypeList;
	}
	

	public List<SelectItem> listResources() {
		try {
			this.resources = new ArrayList<SelectItem>();
			this.resources.add(new SelectItem(null, "..."));
			Resource filter = new Resource();
			filter.setStatus(UserStatusEnum.ACTIVE);
			List<Resource> resourceList = facade.getResourceController().selectBy(filter);
			for (Resource resource : resourceList) {
				this.resources.add(new SelectItem(resource, resource.getName()));
			}

			setResourceDefault();

			return this.resources;

		} catch (ControllerException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<SelectItem> listBusinessLeaders() {
		try {
			this.businessLeaders = new ArrayList<SelectItem>();
			this.businessLeaders.add(new SelectItem(null, "..."));
			List<BusinessLeader> businessLeadersList = facade.getBusinessLeaderController().selectAll();
			for (BusinessLeader businessLeader : businessLeadersList) {
				this.businessLeaders.add(new SelectItem(businessLeader, businessLeader.getName()));
			}
			return this.businessLeaders;
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		return null;
	}

	public String add() {
		action = "Add";
		item = new Story();		
		if (filter.getResource() != null) {
			item.setResource(filter.getResource());
			// CHANGE
			item.setPoints(0);
			item.setStatus(TicketStatusEnum.COMPLETED);
		}
		cleanAlmStories();
		return getFormPath();
	}

	public String edit(Story row) {
		action = "Edit";
		item = row;
		return getFormPath();
	}

	public String cancel() {
		action = "";
		item = new Story();
		return getListPath();
	}

	public String save() {
		if (validate(item)) {
			try {
				item.setNumber(item.getNumber().trim());
				if (item.getId() == null) {
					facade.getStoryController().insert(item);
				} else {
					facade.getStoryController().update(item);
				}
				addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_SUCCESS, "Story");
				item = new Story();
				filter();
				return getListPath();
			} catch (ControllerException e) {
				addWebMessage(e);
			}
		}
		return null;
	}

	private boolean validate(Story story) {
		boolean valid = true;
		if (story.getNumber() == null || story.getNumber().trim().equals("")) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Number");
			valid = false;
		}
		if (story.getPoints() == null || story.getNumber().trim().equals("")) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Points");
			valid = false;
		}
		if (story.getDate() == null) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Date");
			valid = false;
		}
		if (story.getStatus() == null || story.getStatus() == TicketStatusEnum.NONE) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Status");
			valid = false;
		}
		if (story.getResource() == null) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Resource");
			valid = false;
		}
		/*
		 * if (valid && action.equals("Add")) { try { List<Story> listFiltered =
		 * facade.getStoryController().selectBy(item); if
		 * (!listFiltered.isEmpty()) { addMessage(FacesMessage.SEVERITY_ERROR,
		 * Messages.DUPLICATED_ITEM, item.getNumber(),
		 * item.getResource().getName()); valid = false; } } catch
		 * (ControllerException e) { e.printStackTrace(); } }
		 */
		return valid;
	}

	public Converter getResourceConverter() {
		return getEntityConverter(Resource.class, "resource");
	}

	public Converter getBusinessLeaderConverter() {
		return getEntityConverter(BusinessLeader.class, "business-leader");
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	private void setDefaultUserFilter() {
		if(getUser() != null){		
			if("USER".equals(getUser().getRole())){
				Resource resource = getUser().getResource();
				if (resource != null) {
					filter.setResource(resource);
					filter.setBusinessLeader(resource.getBusinessLeader());
				}	
			}
		}		
	}

	public User getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(User loggedUser) {
		this.loggedUser = loggedUser;
	}

	public List<ALMStory> getAlmStories() {
		return almStories;
	}

	public void setAlmStories(List<ALMStory> almStories) {
		this.almStories = almStories;
	}

	public ALMFilter getAlmFilter() {
		return almFilter;
	}

	public void setAlmFilter(ALMFilter almFilter) {
		this.almFilter = almFilter;
	}

	public List<SelectItem> getAlmFilterTypeList() {
		return almFilterTypeList;
	}

	public void setAlmFilterTypeList(List<SelectItem> almFilterTypeList) {
		this.almFilterTypeList = almFilterTypeList;
	}	
	
	public String copyStories() {
		boolean copied = false;
		if(almStories != null){
			for (ALMStory almStory  : almStories) {
				if(almStory.isCopy()){		
					copied = true;
					if (validate(almStory)) {						
						try {
							almStory.setNumber(almStory.getNumber().trim());
							if (almStory.getId() == null) {
								facade.getStoryController().insert(almStory);
							} else {
								facade.getStoryController().update(almStory);
							}							
						} catch (ControllerException e) {
							addWebMessage(e);							
						}
					}					
				}
			}
			
			if(copied){	
				addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_SUCCESS, "Story");
				item = new Story();
				filter();
				return getListPath();
			}else{			
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,"","No stories selected to copy. Please select atleast one.");
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			}			
		}		
		return null;
	}
	
	public String delete(Story row) {
		
		try {			
			
			facade.getStoryController().delete(row.getId());			
			addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_REMOVE_SUCCESS, "Story");
			item = new Story();
			filter();
			return getListPath();
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		
		return null;
	}
	
	
	
	
	

}

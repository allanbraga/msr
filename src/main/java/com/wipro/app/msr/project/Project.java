package com.wipro.app.msr.project;

import com.wipro.app.msr.sow.Sow;
import com.wipro.app.msr.util.interfaces.IPrimaryKey;

public class Project implements IPrimaryKey<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4930707334989671152L;

	private Integer id;
	private String name;
	private String description;
	private String almId;
	private String almName;
	private Sow sow;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
		

	public String getAlmId() {
		return almId;
	}

	public void setAlmId(String almId) {
		this.almId = almId;
	}

	public String getAlmName() {
		return almName;
	}

	public void setAlmName(String almName) {
		this.almName = almName;
	}	

	public Sow getSow() {
		return sow;
	}

	public void setSow(Sow sow) {
		this.sow = sow;
	}

	@Override
	public Integer getPrimaryKey() {
		return id;
	}
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	

}

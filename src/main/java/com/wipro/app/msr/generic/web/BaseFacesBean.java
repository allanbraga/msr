package com.wipro.app.msr.generic.web;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.primefaces.context.RequestContext;

import com.wipro.app.msr.util.constants.SeverityEnum;
import com.wipro.app.msr.util.exceptions.ControllerException;
import com.wipro.app.msr.util.exceptions.ErrorMessage;
import com.wipro.app.msr.util.interfaces.IPrimaryKey;

public abstract class BaseFacesBean<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	protected List<T> list;

	protected T item;
	protected T filter;
	protected String action;
	private static ResourceBundle messages;
	private String module;

	public BaseFacesBean(String module) {
		loadMessages();
		this.module = module;
	}

	private void loadMessages() {
		if (messages == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			Application application = context.getApplication();
			String messageBundleName = application.getMessageBundle();
			Locale local = context.getViewRoot().getLocale();
			messages = ResourceBundle.getBundle(messageBundleName, local, getClass().getClassLoader());
		}
	}

	public void addMessage(Severity severity, String key, String... parameters) {
		if (parameters.length == 0) {
			getContext().addMessage(null, new FacesMessage(severity, messages.getString(key), ""));
		} else {
			MessageFormat formatter = new MessageFormat(messages.getString(key));
			String message = formatter.format(parameters);
			addTextMessage(severity, message);
		}
	}

	public void setError(boolean error) {
		RequestContext.getCurrentInstance().addCallbackParam("error", error);
	}

	public void addTextMessage(Severity severity, String message) {
		getContext().addMessage(null, new FacesMessage(severity, message, ""));
	}

	public void addWebMessage(ControllerException ne) {
		MessageFormat formatter = null;
		try {
			for (ErrorMessage message : ne.getKeys()) {
				formatter = new MessageFormat(messages.getString(message.getKey()));
				Severity severity = getSeverity(message.getSeverity());
				if (message.getValues().length > 0) {
					getContext().addMessage(null, new FacesMessage(severity, formatter.format(message.getValues()), ""));
				} else {
					addMessage(severity, message.getKey());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Severity getSeverity(SeverityEnum severity) {
		switch (severity) {
		case WARNING:
			return FacesMessage.SEVERITY_WARN;
		case ERROR:
			return FacesMessage.SEVERITY_ERROR;
		}
		return FacesMessage.SEVERITY_ERROR;
	}

	public FacesContext getContext() {
		return FacesContext.getCurrentInstance();
	}

	public UIComponent getComponent(String idComponent) {
		return getContext().getViewRoot().findComponent(idComponent);
	}
	
	public void setSessionParameter(String key, Object object) {
		getContext().getExternalContext().getSessionMap().put(key, object);
	}

	public Object getSessionParameter(String key) {
		return getContext().getExternalContext().getSessionMap().get(key);
	}
	
	public <P extends IPrimaryKey<PK>, PK extends Serializable> Converter getEntityConverter(Class<P> entity, String name) {
		Converter converter = (Converter) getSessionParameter("combo:conversor:" + getClass().getSimpleName() + ":" + name);
		if (converter == null) {
			converter = new BaseConverter<P, PK>();
			setSessionParameter("combo:conversor:" + getClass().getSimpleName() + ":" + name, converter);
		}
		return converter;
	}

	@SuppressWarnings("unchecked")
	public Converter getEnumConverter() {
		return new Converter() {
			@SuppressWarnings("rawtypes")
			@Override
			public Object getAsObject(FacesContext context, UIComponent component, String value) {
				if ("-1".equals(value))
					return null;
				else {
					try {
						String values[] = value.split(";");
						String classe = values[0];
						String nome = values[1];
						Class<Enum> clazz = (Class<Enum>) Class.forName(classe);
						return Enum.valueOf(clazz, nome);
					} catch (Exception e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			@SuppressWarnings("rawtypes")
			@Override
			public String getAsString(FacesContext context, UIComponent component, Object obj) {
				if (obj == null)
					return "-1";
				else {
					Enum enumeracao = (Enum) obj;
					Class<Enum> clazz = (Class<Enum>) enumeracao.getClass();
					return clazz.getName() + ";" + enumeracao.name();
				}
			}
		};
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public T getItem() {
		return item;
	}

	public void setItem(T item) {
		this.item = item;
	}
	
	public T getFilter() {
		return filter;
	}

	public void setFilter(T filter) {
		this.filter = filter;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	public String getListPath() {
		return "/pages/" + module+".xhtml";
	}
	
	public String getOtherPath(String name) {
		return "/pages/" + name+".xhtml";
	}
	
	public String getFormPath() {
		return "/pages/" + module + "-form.xhtml";
	}

}

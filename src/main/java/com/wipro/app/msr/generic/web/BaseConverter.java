package com.wipro.app.msr.generic.web;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.wipro.app.msr.util.interfaces.IPrimaryKey;

public class BaseConverter<T extends IPrimaryKey<PK>, PK extends Serializable> implements Converter{
	private Map<String, T> entities = new HashMap<String, T>();
	
	public Object getAsObject(FacesContext context, UIComponent component, String id) {
		if ("-1".equals(id))
			return null;
		return entities.get(id);
	}

	@SuppressWarnings("unchecked")
	public String getAsString(FacesContext context, UIComponent component, Object obj) {
		if (obj == null)
			return "-1";
		if (obj instanceof String && obj.toString().equals("-1"))
			return obj.toString();
		T entity = (T) obj;
		if (entity.getPrimaryKey() == null) {
			return "-1";
		}
		entities.put(entity.getPrimaryKey().toString(), entity);
		return entity.getPrimaryKey().toString();
	}
}
package com.wipro.app.msr.generic.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.wipro.app.msr.util.exceptions.ControllerException;
import com.wipro.app.msr.util.interfaces.IPrimaryKey;

public interface IBaseController<T extends IPrimaryKey<PK>, PK extends Serializable> extends Serializable {

	public T selectById(PK id) throws ControllerException;

	public List<T> selectAll() throws ControllerException;

	public List<T> selectBy(T object) throws ControllerException;
	
	public List<T> selectList(String queryId) throws ControllerException;

	public List<T> selectList(String queryId, Object parameter) throws ControllerException;
	
	public List<T> selectList(String queryId, Map<String, Object> parameter) throws ControllerException;

	public T selectObject(String sentencaMapeada, Object parameter) throws ControllerException;
	
	public void insert(T object) throws ControllerException;

	public void update(T object) throws ControllerException;

	public void delete(PK id) throws ControllerException;
	
}
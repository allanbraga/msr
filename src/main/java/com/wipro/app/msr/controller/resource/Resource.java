package com.wipro.app.msr.controller.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.wipro.app.msr.controller.businessleader.BusinessLeader;
import com.wipro.app.msr.controller.story.Story;
import com.wipro.app.msr.project.Project;
import com.wipro.app.msr.util.constants.RoleEnum;
import com.wipro.app.msr.util.constants.UserStatusEnum;
import com.wipro.app.msr.util.interfaces.IPrimaryKey;

public class Resource implements IPrimaryKey<Integer> {
    
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private String name;
    private BusinessLeader businessLeader;
    private UserStatusEnum status;
    private RoleEnum role;
    private String employeeId;
    private List<Story> stories = new ArrayList<>();
    private Project project;

    public Resource() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserStatusEnum getStatus() {
        return status;
    }

    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }

    public BusinessLeader getBusinessLeader() {
        return businessLeader;
    }

    public void setBusinessLeader(BusinessLeader businessLeader) {
        this.businessLeader = businessLeader;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    public List<Story> getStories() {
        return stories;
    }

    public void setStories(List<Story> stories) {
        this.stories = stories;
    }
    
    
    
    public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	
	

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	@Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Resource other = (Resource) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Resource{" + "id=" + id + ", name=" + name + ", businessLeader=" + businessLeader + ", status=" + status + ", role=" + role + ", stories=" + stories + '}';
    }
    
    @Override
	public Integer getPrimaryKey() {
		return id;
	}

}

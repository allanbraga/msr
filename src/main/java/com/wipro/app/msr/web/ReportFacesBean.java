package com.wipro.app.msr.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.MimetypesFileTypeMap;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.wipro.app.msr.controller.ControllerFacade;
import com.wipro.app.msr.controller.businessleader.BusinessLeader;
import com.wipro.app.msr.controller.story.Story;
import com.wipro.app.msr.generic.web.BaseFacesBean;
import com.wipro.app.msr.job.StoryJob;
import com.wipro.app.msr.report.Report;
import com.wipro.app.msr.sow.Sow;
import com.wipro.app.msr.util.constants.Messages;
import com.wipro.app.msr.util.constants.UserStatusEnum;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller
@ViewScoped
@ManagedBean(name = "reportFacesBean")
public class ReportFacesBean extends BaseFacesBean<Report> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ControllerFacade facade;	
	
	@Autowired
	private Report report;
	
	@Autowired
	StoryJob storyJob;

	private StreamedContent file;
	private Date startDate;
	private Date endDate;
	private HashMap<String, List<Story>> storiesMap = new HashMap<>();
	private HashMap<String,Map<String, Integer>> totalsMap = new HashMap<>();
	private Sow sow;
	
	private List<SelectItem> sowList;
	
	public ReportFacesBean() {
		super("report");
		Calendar calendar = GregorianCalendar.getInstance();
		int month = calendar.get(Calendar.MONTH);
		Calendar calendarStartDate = GregorianCalendar.getInstance();
		calendarStartDate.set(Calendar.MONTH, month - 1);
		calendarStartDate.set(Calendar.DAY_OF_MONTH, 25);
		startDate = calendarStartDate.getTime();
		Calendar calendarEndDate = GregorianCalendar.getInstance();
		calendarEndDate.set(Calendar.DAY_OF_MONTH, 25);
		endDate = calendarEndDate.getTime();
		sow = new Sow();
	}

	public void generate() {
		try {
			List<BusinessLeader> businessLeaders = getActiveBusinessLeaders();
			getStoriesForDateRange(startDate, endDate, businessLeaders);
			File reportFile = report.generateReport(startDate, endDate, businessLeaders, storiesMap, totalsMap);
			addTextMessage(FacesMessage.SEVERITY_INFO, "Report generated!");
			InputStream  stream = new FileInputStream(reportFile);
			file = new DefaultStreamedContent(stream, new MimetypesFileTypeMap().getContentType(reportFile), reportFile.getName());
		} catch (FileNotFoundException e) {
			addTextMessage(FacesMessage.SEVERITY_ERROR, Messages.DOWNLOAD_ERROR);
		} catch (ControllerException e) {
			addWebMessage(e);
		}
	}
	
	
	public void startJob(){
		try{
			Map<String, Integer> response = storyJob.copyAlmStories();
			addTextMessage(FacesMessage.SEVERITY_INFO, "Job finished! were processed "+response.get("VALID_STORIES")+" valid Stories/Defect and "+response.get("INVALID_STORIES")+" invalid Sories/Defects ");	
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));;
		}				
	}
	
	private List<BusinessLeader> getActiveBusinessLeaders() throws ControllerException {
		BusinessLeader filter = new BusinessLeader();
		filter.setStatus(UserStatusEnum.ACTIVE);
		filter.setSow(sow);
		return facade.getBusinessLeaderController().selectBy(filter);
	}
	
	private void getStoriesForDateRange(Date startDate, Date endDate, List<BusinessLeader> businessLeaders) throws ControllerException {
		List<Story> stories = facade.getStoryController().getStoriesByDateRange(startDate, endDate , sow.getId());
		List<Story> storiesCounts = facade.getStoryController().getStoriesCountByDateRangeForBusinessLeader(startDate, endDate ,sow.getId());
		List<Story> storiesPoints = facade.getStoryController().getStoriesPointsByDateRangeForBusinessLeader(startDate, endDate, sow.getId());
		List<Story> storiesCompletedPoints = facade.getStoryController().getStoriesCompletedPointsByDateRangeForBusinessLeader(startDate, endDate, sow.getId());
		for (BusinessLeader businessLeader : businessLeaders) {
			List<Story> list = new ArrayList<>();
			for (Story story : stories) {
				if (story.getBusinessLeader().equals(businessLeader)) {
					list.add(story);
				}
			}
			
			if (totalsMap.get(businessLeader.getName() + "_count_stories") == null) {
				totalsMap.put(businessLeader.getName() + "_count_stories", new HashMap<String,Integer>());
			}
			
			for (Story story : storiesCounts) {
				if (story.getBusinessLeader().equals(businessLeader)) {					
					totalsMap.get(businessLeader.getName() + "_count_stories").put(story.getProject().getName(), story.getPoints());
				}
			}
			
			if (totalsMap.get(businessLeader.getName() + "_count_points") == null) {
				totalsMap.put(businessLeader.getName() + "_count_points", new HashMap<String,Integer>());
			}
			
			for (Story story : storiesPoints) {
				if (story.getBusinessLeader().equals(businessLeader)) {
					totalsMap.get(businessLeader.getName() + "_count_points").put(story.getProject().getName(), story.getPoints());
				}
			}
			
			if (totalsMap.get(businessLeader.getName() + "_count_completed_points") == null) {
				totalsMap.put(businessLeader.getName() + "_count_completed_points", new HashMap<String,Integer>());
			}
			
			for (Story story : storiesCompletedPoints) {
				if (story.getBusinessLeader().equals(businessLeader)) {
					totalsMap.get(businessLeader.getName() + "_count_completed_points").put(story.getProject().getName(), story.getPoints());
				}
			}
			
			storiesMap.put(businessLeader.getName(), list);
		}
	}
	
	public StreamedContent getFile() {
		return file;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
			
	public Sow getSow() {
		return sow;
	}

	public void setSow(Sow sow) {
		this.sow = sow;
	}

	public List<SelectItem> listSow() {
		try {
			this.sowList = new ArrayList<SelectItem>();
			this.sowList.add(new SelectItem(null,"..."));
			List<Sow> sowList = facade.getSowController().selectAll();
			for (Sow sow : sowList) {
				this.sowList.add(new SelectItem(sow, sow.getName()));
			}
			return this.sowList;
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		return null;
	}
	
	
	public Converter getSowConverter() {
		return getEntityConverter(Sow.class, "sow");
	}
	

}

package com.wipro.app.msr.util.exceptions;

import java.io.Serializable;

import com.wipro.app.msr.util.constants.SeverityEnum;

public class ErrorMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private String key;

	private String[] values;

	private SeverityEnum severity;

	@Deprecated
	public ErrorMessage() {
	}

	public ErrorMessage(SeverityEnum severity, String key, String... values) {
		this.severity = severity;
		this.key = key;
		this.values = values;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String[] getValues() {
		return values;
	}

	public void setValues(String[] values) {
		this.values = values;
	}

	public SeverityEnum getSeverity() {
		return severity;
	}

	public void setSeverity(SeverityEnum severity) {
		this.severity = severity;
	}
	
}
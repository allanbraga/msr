package com.wipro.app.msr.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.wipro.app.msr.controller.ControllerFacade;
import com.wipro.app.msr.controller.businessleader.BusinessLeader;
import com.wipro.app.msr.generic.web.BaseFacesBean;
import com.wipro.app.msr.sow.Sow;
import com.wipro.app.msr.util.constants.Messages;
import com.wipro.app.msr.util.constants.UserStatusEnum;
import com.wipro.app.msr.util.exceptions.ControllerException;


@Controller
@Scope("session")
@ManagedBean(name = "businessLeaderFacesBean")
public class BusinessLeaderFacesBean extends BaseFacesBean<BusinessLeader> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ControllerFacade facade;
	
	private List<SelectItem> status;
	
	private List<SelectItem> sowList;
	
	public BusinessLeaderFacesBean() {
		super("business-leader");
		item = new BusinessLeader();
	}
	
	public void start() {
		try {
			list = facade.getBusinessLeaderController().selectAll();
		} catch (ControllerException e) {
			e.printStackTrace();
		}
	}
	
	public List<SelectItem> listStatus() {
		this.status = new ArrayList<SelectItem>();
		for (UserStatusEnum status : UserStatusEnum.values()) {
			this.status.add(new SelectItem(status, status.toString()));
		}
		return this.status;
	}
	
	public String add() {
		action = "Add";
		item = new BusinessLeader();
		return getFormPath();
	}
	
	public String edit(BusinessLeader row) {
		action = "Edit";
		item = row;
		return getFormPath();
	}
	
	public String cancel() {
		action = "";
		item = new BusinessLeader();
		return getListPath();
	}
	
	public String save() {
		if (validate()) {
			try {
				if (item.getId() == null) {
					facade.getBusinessLeaderController().insert(item);
				} else {
					facade.getBusinessLeaderController().update(item);
				}
				addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_SUCCESS, "Business Leader");
				start();
				return getListPath();
			} catch (ControllerException e) {
				addWebMessage(e);
			}
		}
		return null;
	}
	
	private boolean validate() {
		boolean valid = true;
		if (item.getName() == null || item.getName().trim().equals("")) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Name");
			valid = false;
		}
		
		if (item.getSow() == null) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Sow");
			valid = false;
		}
		
		if (item.getStatus() == null || item.getStatus() == UserStatusEnum.NONE) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Status");
			valid = false;
		}
		return valid;
	}
	
	public List<SelectItem> listSow() {
		try {
			this.sowList = new ArrayList<SelectItem>();
			this.sowList.add(new SelectItem(null,"..."));
			List<Sow> sowList = facade.getSowController().selectAll();
			for (Sow sow : sowList) {
				this.sowList.add(new SelectItem(sow, sow.getName()));
			}
			return this.sowList;
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		return null;
	}
	
	public Converter getSowConverter() {
		return getEntityConverter(Sow.class, "sow");
	}
	
	
}

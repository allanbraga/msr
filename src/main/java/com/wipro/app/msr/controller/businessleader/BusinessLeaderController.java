package com.wipro.app.msr.controller.businessleader;

import org.springframework.stereotype.Controller;

import com.wipro.app.msr.generic.controller.BaseController;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller
public class BusinessLeaderController extends BaseController<BusinessLeader, Integer> implements IBusinessLeaderController {
	
	private static final long serialVersionUID = 1L;

	public BusinessLeaderController() {
		super(BusinessLeader.class);
	}
	
	@Override
	public void delete(Integer id) throws ControllerException {
		throw new ControllerException("Method not supported.");
	}

}

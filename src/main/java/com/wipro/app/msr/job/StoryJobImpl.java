package com.wipro.app.msr.job;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wipro.app.msr.alm.ALMFilter;
import com.wipro.app.msr.alm.ALMService;
import com.wipro.app.msr.alm.ALMStory;
import com.wipro.app.msr.alm.FilterTypeEnum;
import com.wipro.app.msr.controller.resource.IResourceController;
import com.wipro.app.msr.controller.resource.Resource;
import com.wipro.app.msr.controller.story.IStoryController;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Service
public class StoryJobImpl implements StoryJob {

	@Autowired
	ALMService almservice;

	@Autowired
	private IResourceController resourceController;

	@Autowired
	private IStoryController storyController;

	@Override
	@Transactional(rollbackFor = ControllerException.class)
	public Map<String,Integer> copyAlmStories() {
		int numberStories = 0;
		int numberInvalidStories = 0;
		Map<String,Integer> response = new HashMap<String,Integer>();
		try {

			List<Resource> resources = resourceController.selectAll();			
			List<ALMStory> storiesToSave = new ArrayList<ALMStory>();
			ALMFilter almFilter = new ALMFilter();
			almFilter.setStartDate(getStartDate());
			almFilter.setEndDate(getEndDate());
			if (resources != null && !resources.isEmpty()) {
				for (Resource resource : resources) {
					
					storiesToSave = new ArrayList<>();
					
					if(resource.getEmployeeId() != null && !resource.getEmployeeId().isEmpty()){
						System.out.println("EMP ID: "+resource.getEmployeeId() + "@mastercard.com");
						almFilter.setUserId(resource.getEmployeeId() + "@mastercard.com");						
						almFilter.setFilterTypeEnum(FilterTypeEnum.CONVERSATION_POST);
						List<ALMStory> almStoriesByConversationPost = almservice.listStoriesByConversationPost(almFilter);
						if (almStoriesByConversationPost != null && !almStoriesByConversationPost.isEmpty()) {						
							for (ALMStory almStoryConversation : almStoriesByConversationPost) {
								almStoryConversation.setNumber(almStoryConversation.getNumber().trim());
								if(!storyContains(almStoryConversation.getNumber(),storiesToSave)){
									storiesToSave.add(almStoryConversation);
								}
							}
						}
						
						almFilter.setFilterTypeEnum(FilterTypeEnum.OWNER_ID);
						List<ALMStory> almStoriesByOwner = almservice.listStoriesByOwner(almFilter);
						if (almStoriesByOwner != null && !almStoriesByOwner.isEmpty()) {						
							for (ALMStory almStoryOwner : almStoriesByOwner) {
								almStoryOwner.setNumber(almStoryOwner.getNumber().trim());
								if(!storyContains(almStoryOwner.getNumber(),storiesToSave)){
									storiesToSave.add(almStoryOwner);
								}
							}						
							
						}
							
						List<ALMStory> almStoriesTestCaseResult = almservice.listStoriesByTestCaseResult(almFilter);
						if (almStoriesTestCaseResult != null && !almStoriesTestCaseResult.isEmpty()) {						
							for (ALMStory almStoryTestCaseResult : almStoriesTestCaseResult) {
								almStoryTestCaseResult.setNumber(almStoryTestCaseResult.getNumber().trim());
								if(!storyContains(almStoryTestCaseResult.getNumber(),storiesToSave)){
									storiesToSave.add(almStoryTestCaseResult);
								}
							}						
							
						}
						
						if(storiesToSave != null && !storiesToSave.isEmpty()){						
							for (ALMStory almStory : storiesToSave) {
								almStory.setResource(resource);
								almStory.setBusinessLeader(resource.getBusinessLeader());								
								if(isStoryValid(almStory)){
									storyController.insert(almStory);
									numberStories++;
								}else{
									numberInvalidStories++;
								}
							}
						}
					}
				}
					
			}
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.put("VALID_STORIES", numberStories);
		response.put("INVALID_STORIES", numberInvalidStories);
		
		return response;

	}

	private Date getStartDate() {
		Calendar calendar = Calendar.getInstance();		
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH)-1);		
		calendar.set(Calendar.DAY_OF_MONTH, 25);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();

	}

	private Date getEndDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 25);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	private boolean storyContains(String storyNumber, List<ALMStory> stories) {

		boolean contains = false;
		if (stories != null) {
			for (ALMStory story : stories) {
				if (story.getNumber().equals(storyNumber)) {
					contains = true;
					break;
				}
			}
		}
		return contains;

	}
	
	private boolean isStoryValid(ALMStory almStory){
		boolean isvalid = true;
		if(almStory.getNumber() == null || almStory.getNumber().isEmpty()){
			isvalid = false;
		}else if(almStory.getDate() == null){
			isvalid = false;
		}else if(almStory.getResource() == null){
			isvalid = false;
		}else if(almStory.getBusinessLeader() == null){
			isvalid = false;
		}else if(almStory.getPoints() == null){
			isvalid = false;
		}	
		return isvalid;
	}

}

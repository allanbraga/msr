package com.wipro.app.msr.alm;

public enum FilterTypeEnum {
	OWNER_ID("Owner ID"), CONVERSATION_POST("Conversation Post");

	final String name;

	FilterTypeEnum(String name) {
		this.name = name;

	}

	public String toString() {
		return name;
	}
}

package com.wipro.app.msr.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.app.msr.controller.businessleader.IBusinessLeaderController;
import com.wipro.app.msr.controller.resource.IResourceController;
import com.wipro.app.msr.controller.story.IStoryController;
import com.wipro.app.msr.project.controller.IProjectController;
import com.wipro.app.msr.sow.controller.ISowController;
import com.wipro.app.msr.user.controller.IUserController;

@Service
public class ControllerFacade implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private IBusinessLeaderController businessLeaderController;

	@Autowired
	private IResourceController resourceController;
	
	@Autowired
	private IStoryController storyController;
	
	@Autowired
	private IUserController userController ;
	
	@Autowired
	private IProjectController projectController;
	
	@Autowired
	private ISowController sowController;	
	

	public IBusinessLeaderController getBusinessLeaderController() {
		return businessLeaderController;
	}

	public void setBusinessLeaderController(IBusinessLeaderController businessLeaderController) {
		this.businessLeaderController = businessLeaderController;
	}

	public IResourceController getResourceController() {
		return resourceController;
	}

	public void setResourceController(IResourceController resourceController) {
		this.resourceController = resourceController;
	}

	public IStoryController getStoryController() {
		return storyController;
	}

	public void setStoryController(IStoryController storyController) {
		this.storyController = storyController;
	}

	public IUserController getUserController() {
		return userController;
	}

	public void setUserController(IUserController userController) {
		this.userController = userController;
	}

	public IProjectController getProjectController() {
		return projectController;
	}

	public void setProjectController(IProjectController projectController) {
		this.projectController = projectController;
	}

	public ISowController getSowController() {
		return sowController;
	}

	public void setSowController(ISowController sowController) {
		this.sowController = sowController;
	}
	
	
	
	
	
	
	
}

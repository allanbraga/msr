package com.wipro.app.msr.web;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.wipro.app.msr.controller.ControllerFacade;
import com.wipro.app.msr.generic.web.BaseFacesBean;
import com.wipro.app.msr.project.Project;
import com.wipro.app.msr.sow.Sow;
import com.wipro.app.msr.util.constants.Messages;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller
@Scope("session")
@ManagedBean(name = "sowFacesBean")
public class SowFacesBean extends BaseFacesBean<Sow> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1611594415563779483L;
	
	
	@Autowired
	private ControllerFacade facade;

	public SowFacesBean() {
		super("sow");
		item = new Sow();
	}

	public void clean() {
		try {
			list = facade.getSowController().selectAll();
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String save() {
		try {
			if (item.getId() == null) {
				facade.getSowController().insert(item);
			} else {
				facade.getSowController().update(item);
			}
			addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_SUCCESS, "Sow");
			item = new Sow();
			clean();
			return getListPath();
		} catch (ControllerException e) {
			addWebMessage(e);
		}

		return null;
	}

	public String add() {
		action = "Add";
		item = new Sow();
		return getFormPath();
	}

	public String edit(Sow row) {
		action = "Edit";
		item = row;
		return getFormPath();
	}

	public String cancel() {
		action = "";
		item = new Sow();
		return getListPath();
	}
	
	public void delete(Sow row) {		
		try {				
			facade.getSowController().delete(row.getId());				
			addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_REMOVE_SUCCESS, "Sow");	
			clean();
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		
	}
}

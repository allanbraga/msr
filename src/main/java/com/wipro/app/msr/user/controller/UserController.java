package com.wipro.app.msr.user.controller;

import org.springframework.stereotype.Controller;

import com.wipro.app.msr.generic.controller.BaseController;
import com.wipro.app.msr.user.User;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller("userController")
public class UserController extends BaseController<User, Integer> implements IUserController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6038761975889865428L;
	
	public UserController() {
		super(User.class);
	}
	
	@Override
	public User getUserByLogin(String login) throws ControllerException {
		return selectObject("select-by-login", login);
	}
}

package com.wipro.app.msr.util.constants;

public enum TicketStatusEnum {
 
    NONE("..."), PROGRESS("In Progress"), HOLD("On Hold"), COMPLETED("Completed");
    
    private final String name;
    
    private TicketStatusEnum(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public String toString() {
        return name;
    }
    
    public static TicketStatusEnum getByName(String name) {
        for (TicketStatusEnum status : TicketStatusEnum.values()) {
            if (status.getName().equals(name)) {
                return status;
            }
        }
        return null;
    } 
    
}

package com.wipro.app.msr.alm;

import java.io.Serializable;

import com.wipro.app.msr.controller.story.Story;

public class ALMStory extends Story implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4982666431791760738L;
	
	private String description;
	private boolean copy;
	private String almStatus;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isCopy() {
		return copy;
	}

	public void setCopy(boolean copy) {
		this.copy = copy;
	}

	public String getAlmStatus() {
		return almStatus;
	}

	public void setAlmStatus(String almStatus) {
		this.almStatus = almStatus;
	}
	
	

}

package com.wipro.app.msr.user.controller;

import com.wipro.app.msr.generic.controller.IBaseController;
import com.wipro.app.msr.user.User;
import com.wipro.app.msr.util.exceptions.ControllerException;

public interface IUserController extends IBaseController<User, Integer> {
	
	public User getUserByLogin(String login) throws ControllerException;
	

}

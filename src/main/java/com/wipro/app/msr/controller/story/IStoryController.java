package com.wipro.app.msr.controller.story;

import java.util.Date;
import java.util.List;

import com.wipro.app.msr.alm.ALMFilter;
import com.wipro.app.msr.alm.ALMStory;
import com.wipro.app.msr.generic.controller.IBaseController;
import com.wipro.app.msr.util.exceptions.ControllerException;

public interface IStoryController extends IBaseController<Story, Integer> {
	
	public List<Story> getStoriesByDateRange(Date startDate, Date endDate, Integer sowId) throws ControllerException;
	
	public List<Story> getStoriesCountByDateRangeForBusinessLeader(Date startDate, Date endDate , Integer sowId) throws ControllerException;
	
	public List<Story> getStoriesPointsByDateRangeForBusinessLeader(Date startDate, Date endDate , Integer sowId) throws ControllerException;
	
	public List<Story> getStoriesCompletedPointsByDateRangeForBusinessLeader(Date startDate, Date endDate , Integer sowId) throws ControllerException;
	
	public List<ALMStory> getALMStories(ALMFilter almFilter) throws ControllerException;
	
}

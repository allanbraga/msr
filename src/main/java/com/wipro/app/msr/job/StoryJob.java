package com.wipro.app.msr.job;

import java.util.Map;

public interface StoryJob {
	
	Map<String,Integer> copyAlmStories();

}

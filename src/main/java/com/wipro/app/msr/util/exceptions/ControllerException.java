package com.wipro.app.msr.util.exceptions;

import java.util.ArrayList;
import java.util.List;

import com.wipro.app.msr.util.constants.SeverityEnum;

public class ControllerException extends Exception {

	private static final long serialVersionUID = 1L;
	private List<ErrorMessage> messages;

	protected ControllerException() {
		messages = new ArrayList<ErrorMessage>();
	}

	public ControllerException(String message) {
		this(message, (Object[]) null);
	}

	public ControllerException(SeverityEnum severity, String message) {
		this(severity, message, (Object[]) null);
	}

	public ControllerException(String message, Object[] parametros) {
		this(SeverityEnum.ERROR, message, parametros);
	}

	public ControllerException(SeverityEnum severity, String message, Object[] parametros) {
		this();
		ErrorMessage msg;
		if (parametros != null && parametros.length > 0) {
			String[] p = new String[parametros.length];
			for (int i = 0; i < parametros.length; i++) {
				p[i] = parametros[i].toString();
			}
			msg = new ErrorMessage(severity, message, p);
		} else {
			msg = new ErrorMessage(severity, message);
		}
		messages.add(msg);
	}

	public ControllerException(List<ErrorMessage> mensagens) {
		this();
		this.messages = mensagens;
	}

	public ControllerException(Throwable exception) {
		this(SeverityEnum.ERROR, exception);
	}

	public ControllerException(SeverityEnum severity, Throwable exception) {
		super(exception);
		ErrorMessage message = null;
		messages = new ArrayList<ErrorMessage>();
		if (exception.getMessage() != null) {
			message = new ErrorMessage(severity, exception.getMessage());
		} else if (exception.getCause() != null && exception.getCause().getMessage() != null) {
			message = new ErrorMessage(severity, exception.getCause().getMessage());
		} 
		if (message != null)
			messages.add(message);
	}

	public ControllerException(String message, Throwable exception) {
		this(SeverityEnum.ERROR, message, exception);
	}

	public ControllerException(SeverityEnum severity, String message, Throwable exception) {
		super(exception);
		ErrorMessage msg = null;
		messages = new ArrayList<ErrorMessage>();
		if (!"".equals(message)) {
			msg = new ErrorMessage(severity, message);
		} else if (exception.getMessage() != null) {
			msg = new ErrorMessage(severity, exception.getMessage());
		} else if (exception.getCause() != null && exception.getCause().getMessage() != null) {
			msg = new ErrorMessage(severity, exception.getCause().getMessage());
		}
		if (message != null)
			messages.add(msg);
	}

	public List<ErrorMessage> getKeys() {
		return messages;
	}
}

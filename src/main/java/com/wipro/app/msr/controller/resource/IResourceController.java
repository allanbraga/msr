package com.wipro.app.msr.controller.resource;

import java.util.List;

import com.wipro.app.msr.generic.controller.IBaseController;
import com.wipro.app.msr.util.exceptions.ControllerException;

public interface IResourceController extends IBaseController<Resource, Integer> {

	public List<Resource> getResourceByBusinessLeader(Integer businessLeaderId) throws ControllerException;
	
	public Resource getResourceByName(String name) throws ControllerException;
	
}

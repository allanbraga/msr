package com.wipro.app.msr.report;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.wipro.app.msr.controller.businessleader.BusinessLeader;
import com.wipro.app.msr.controller.resource.Resource;
import com.wipro.app.msr.controller.story.Story;

@Service
public class Report {

	private HSSFWorkbook workbook;
	private Font font;

	public File generateReport(Date startDate, Date endDate, List<BusinessLeader> businessLeaders,
			HashMap<String, List<Story>> storiesMap, HashMap<String, Map<String, Integer>> totalsMap) {
		try {
			Report report = new Report();
			report.createWorkbook();
			HSSFSheet homeSheet = report.createSheet("HOME");
			report.createHomeDateInterval(homeSheet, startDate, endDate);
			report.createHomeBusinessLeadersTable(homeSheet, businessLeaders, totalsMap);
			HSSFSheet teamSheet = report.createSheet("TEAM");
			report.createTeamResourcesTable(teamSheet, businessLeaders, storiesMap);
			for (BusinessLeader businessLeader : businessLeaders) {
				HSSFSheet businessLeaderSheet = report.createSheet(businessLeader.getName());
				report.createBusinessLeaderSheetTable(businessLeaderSheet, businessLeader,
						storiesMap.get(businessLeader.getName()));
			}
			return report.writeFile(startDate, endDate);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private Workbook createWorkbook() {
		workbook = new HSSFWorkbook();
		return workbook;
	}

	private HSSFSheet createSheet(String name) {
		return workbook.createSheet(name);
	}

	private void autoFormatSheetColumns(HSSFSheet sheet, Integer columnCount) {
		for (int index = 0; index < columnCount; index++) {
			sheet.autoSizeColumn(index);
		}
	}

	private Font getFont() {
		if (font == null) {
			font = workbook.createFont();
			font.setFontHeightInPoints((short) 11);
			font.setFontName("Calibri");
		}
		return font;
	}

	private File writeFile(Date startDate, Date endDate) {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddyyyy");
			String path = "C:/Data/Wipro/msr/";

			File file = new File(path + "MSR_Template_" + simpleDateFormat.format(startDate) + "_"
					+ simpleDateFormat.format(endDate) + ".xls");

			FileOutputStream fileOutputStream = new FileOutputStream(file);
			workbook.write(fileOutputStream);
			fileOutputStream.close();
			workbook.close();
			return file;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private void createHomeDateInterval(HSSFSheet sheet, Date startDate, Date endDate) {
		HSSFRow row = sheet.createRow((short) 0);
		// Date Interval
		row.createCell(0).setCellValue("Date Interval");
		row.createCell(1).setCellValue(startDate);
		row.createCell(2).setCellValue(endDate);
		// Style
		HSSFCellStyle styleDateInterval = workbook.createCellStyle();
		styleDateInterval.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		styleDateInterval.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleDateInterval.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		styleDateInterval.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleDateInterval.setFont(getFont());
		row.getCell(0).setCellStyle(styleDateInterval);
		HSSFDataFormat dataFormat = workbook.createDataFormat();
		HSSFCellStyle styleStartDate = workbook.createCellStyle();
		styleStartDate.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		styleStartDate.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleStartDate.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		styleStartDate.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleStartDate.setDataFormat(dataFormat.getFormat("dd MMM yyyy"));
		styleStartDate.setFont(getFont());
		row.getCell(1).setCellStyle(styleStartDate);
		HSSFCellStyle styleEndDate = workbook.createCellStyle();
		styleEndDate.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		styleEndDate.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		styleEndDate.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		styleEndDate.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleEndDate.setDataFormat(dataFormat.getFormat("dd MMM yyyy"));
		styleEndDate.setFont(getFont());
		row.getCell(2).setCellStyle(styleEndDate);
		autoFormatSheetColumns(sheet, 3);
	}

	private void createTeamResourcesTable(HSSFSheet sheet, List<BusinessLeader> list,
			HashMap<String, List<Story>> storiesMap) {
		int indexColumn = 0;
		// Styles
		HSSFCellStyle styleHeader = workbook.createCellStyle();
		styleHeader.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleHeader.setFont(getFont());
		styleHeader.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
		styleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		HSSFCellStyle styleItem = workbook.createCellStyle();
		styleItem.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		styleItem.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleItem.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleItem.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleItem.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		styleItem.setFont(getFont());
		HSSFCellStyle styleItemNoData = workbook.createCellStyle();
		styleItemNoData.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		styleItemNoData.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleItemNoData.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleItemNoData.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleItemNoData.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		styleItemNoData.setFont(getFont());
		for (BusinessLeader businessLeader : list) {
			int indexRow = 0;
			HSSFRow rowHeader;
			if (sheet.getRow(indexRow) == null) {
				rowHeader = sheet.createRow((short) indexRow);
			} else {
				rowHeader = sheet.getRow(indexRow);
			}
			rowHeader.createCell(indexColumn).setCellValue(businessLeader.getName());
			rowHeader.getCell(indexColumn).setCellStyle(styleHeader);
			indexRow++;
			for (Resource resource : businessLeader.getResources()) {
				HSSFRow row;
				if (sheet.getRow(indexRow) == null) {
					row = sheet.createRow((short) indexRow);
				} else {
					row = sheet.getRow(indexRow);
				}
				row.createCell(indexColumn).setCellValue(resource.getName());
				if (hasDataOnReport(resource, businessLeader, storiesMap)) {
					row.getCell(indexColumn).setCellStyle(styleItem);
				} else {
					row.getCell(indexColumn).setCellStyle(styleItemNoData);
				}
				indexRow++;
			}
			indexColumn++;
		}
		autoFormatSheetColumns(sheet, indexColumn);
	}

	private void createHomeBusinessLeadersTable(HSSFSheet sheet, List<BusinessLeader> list,
			HashMap<String, Map<String, Integer>> totalsMap) {
		// Headers
		HSSFRow rowHeader = sheet.createRow((short) 2);
		rowHeader.createCell(0).setCellValue("Business Leader / Project");
		rowHeader.createCell(1).setCellValue("Number of User Stories/Defects");
		rowHeader.createCell(2).setCellValue("Cumulative Story Points");
		rowHeader.createCell(3).setCellValue("Cumulative Completed Story Points");
		rowHeader.createCell(4).setCellValue("Number of Resources");
		// Style
		HSSFCellStyle styleHeader = workbook.createCellStyle();
		styleHeader.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		styleHeader.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		styleHeader.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleHeader.setFont(getFont());
		styleHeader.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
		styleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		rowHeader.getCell(0).setCellStyle(styleHeader);
		rowHeader.getCell(1).setCellStyle(styleHeader);
		rowHeader.getCell(2).setCellStyle(styleHeader);
		rowHeader.getCell(3).setCellStyle(styleHeader);
		HSSFCellStyle styleHeaderLast = workbook.createCellStyle();
		styleHeaderLast.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		styleHeaderLast.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		styleHeaderLast.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		styleHeaderLast.setFont(getFont());
		styleHeaderLast.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
		styleHeaderLast.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		rowHeader.getCell(4).setCellStyle(styleHeaderLast);

		// Style
		HSSFCellStyle styleTrailer = workbook.createCellStyle();
		styleTrailer.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		styleTrailer.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleTrailer.setFont(getFont());
		styleTrailer.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		styleTrailer.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);		
		HSSFCellStyle styleTrailerLast = workbook.createCellStyle();
		styleTrailerLast.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		styleTrailerLast.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		styleTrailerLast.setFont(getFont());
		styleTrailerLast.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		styleTrailerLast.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		// Business Leaders
		int totalResources = 0;
		int totalStories = 0;
		int totalPoints = 0;
		int totalCompletedPoints = 0;
		int rowNumber = 3;
		Map<String, ProjectInformation> mapProjectInfo;
		for (BusinessLeader businessLeader : list) {
			int totalBLStories = 0;
			int totalBLPoints = 0;
			int totalBLCompletedPoints = 0;
			mapProjectInfo = new HashMap<String, ProjectInformation>();

			// loop by projects
			for (Map.Entry<String, Integer> entry : totalsMap.get(businessLeader.getName() + "_count_stories")
					.entrySet()) {
				totalStories += entry.getValue();
				totalBLStories += entry.getValue();
				if (mapProjectInfo.get(entry.getKey()) == null) {
					mapProjectInfo.put(entry.getKey(), new ProjectInformation());
				}
				mapProjectInfo.get(entry.getKey()).setProjectName(entry.getKey());
				mapProjectInfo.get(entry.getKey()).setTotalStories(entry.getValue());
			}

			for (Map.Entry<String, Integer> entry : totalsMap.get(businessLeader.getName() + "_count_points")
					.entrySet()) {
				totalPoints += entry.getValue();
				totalBLPoints += entry.getValue();
				if (mapProjectInfo.get(entry.getKey()) == null) {
					mapProjectInfo.put(entry.getKey(), new ProjectInformation());
				}
				mapProjectInfo.get(entry.getKey()).setProjectName(entry.getKey());
				mapProjectInfo.get(entry.getKey()).setTotalPoints(entry.getValue());

			}

			for (Map.Entry<String, Integer> entry : totalsMap.get(businessLeader.getName() + "_count_completed_points")
					.entrySet()) {
				totalCompletedPoints += entry.getValue();
				totalBLCompletedPoints += entry.getValue();

				if (mapProjectInfo.get(entry.getKey()) == null) {
					mapProjectInfo.put(entry.getKey(), new ProjectInformation());
				}
				mapProjectInfo.get(entry.getKey()).setProjectName(entry.getKey());
				mapProjectInfo.get(entry.getKey()).setTotalCompleted(entry.getValue());
			}

			totalResources += businessLeader.getResources().size();

			HSSFRow row = sheet.createRow((short) rowNumber);

			row.createCell(0).setCellValue(businessLeader.getName());
			row.createCell(1).setCellValue(totalBLStories);
			row.createCell(2).setCellValue(totalBLPoints);
			row.createCell(3).setCellValue(totalBLCompletedPoints);
			row.createCell(4).setCellValue(businessLeader.getResources().size());
			// Style
			HSSFCellStyle styleItem = workbook.createCellStyle();
			styleItem.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			styleItem.setBorderRight(HSSFCellStyle.BORDER_THIN);
			styleItem.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
			styleItem.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			styleItem.setFont(getFont());
			row.getCell(0).setCellStyle(styleTrailer);
			row.getCell(1).setCellStyle(styleTrailer);
			row.getCell(2).setCellStyle(styleTrailer);
			row.getCell(3).setCellStyle(styleTrailer);
			row.getCell(4).setCellStyle(styleTrailerLast);

			HSSFCellStyle styleItemLast = workbook.createCellStyle();
			styleItemLast.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			styleItemLast.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
			styleItemLast.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
			styleItemLast.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			styleItemLast.setFont(getFont());
			rowNumber++;

			for (Map.Entry<String, ProjectInformation> entry : mapProjectInfo.entrySet()) {
				if(entry.getValue() != null){
					row = sheet.createRow((short) rowNumber);
					row.createCell(0).setCellValue(entry.getKey());
					row.createCell(1).setCellValue(entry.getValue().getTotalStories() != null ? entry.getValue().getTotalStories() : 0 );
					row.createCell(2).setCellValue(entry.getValue().getTotalPoints() != null ? entry.getValue().getTotalPoints() : 0 );
					row.createCell(3).setCellValue(entry.getValue().getTotalCompleted() != null ? entry.getValue().getTotalCompleted() : 0 );
					row.createCell(4).setCellValue("");
					styleItem.setBorderBottom(HSSFCellStyle.BORDER_THIN);
					styleItem.setBorderRight(HSSFCellStyle.BORDER_THIN);
					styleItem.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
					styleItem.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
					styleItem.setFont(getFont());
					row.getCell(0).setCellStyle(styleItem);
					row.getCell(1).setCellStyle(styleItem);
					row.getCell(2).setCellStyle(styleItem);
					row.getCell(3).setCellStyle(styleItem);
					row.getCell(4).setCellStyle(styleItemLast);
					rowNumber++;	
				}				

			}

		}
		// Trailers
		HSSFRow rowTrailer = sheet.createRow((short) rowNumber);
		rowTrailer.createCell(0).setCellValue("TOTAL");
		rowTrailer.createCell(1).setCellValue(totalStories);
		rowTrailer.createCell(2).setCellValue(totalPoints);
		rowTrailer.createCell(3).setCellValue(totalCompletedPoints);
		rowTrailer.createCell(4).setCellValue(totalResources);		
		rowTrailer.getCell(0).setCellStyle(styleHeader);
		rowTrailer.getCell(1).setCellStyle(styleHeader);
		rowTrailer.getCell(2).setCellStyle(styleHeader);
		rowTrailer.getCell(3).setCellStyle(styleHeader);
		rowTrailer.getCell(4).setCellStyle(styleHeaderLast);
		autoFormatSheetColumns(sheet, 5);
	}

	private void createBusinessLeaderSheetTable(HSSFSheet sheet, BusinessLeader businessLeader, List<Story> list) {
		// Headers
		HSSFRow rowHeader = sheet.createRow((short) 0);
		rowHeader.createCell(0).setCellValue("Date");
		rowHeader.createCell(1).setCellValue("BL");
		rowHeader.createCell(2).setCellValue("Project");
		rowHeader.createCell(3).setCellValue("Resource Name");
		rowHeader.createCell(4).setCellValue("Role");
		rowHeader.createCell(5).setCellValue("Story#");
		rowHeader.createCell(6).setCellValue("Story Points");
		rowHeader.createCell(7).setCellValue("Status");
		rowHeader.createCell(8).setCellValue("Comments");
		// Style
		HSSFCellStyle styleHeader = workbook.createCellStyle();
		styleHeader.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleHeader.setFont(getFont());
		styleHeader.setFillForegroundColor(HSSFColor.SKY_BLUE.index);
		styleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleHeader.setAlignment(CellStyle.ALIGN_CENTER);
		rowHeader.getCell(0).setCellStyle(styleHeader);
		rowHeader.getCell(1).setCellStyle(styleHeader);
		rowHeader.getCell(2).setCellStyle(styleHeader);
		rowHeader.getCell(3).setCellStyle(styleHeader);
		rowHeader.getCell(4).setCellStyle(styleHeader);
		rowHeader.getCell(5).setCellStyle(styleHeader);
		rowHeader.getCell(6).setCellStyle(styleHeader);
		rowHeader.getCell(7).setCellStyle(styleHeader);
		rowHeader.getCell(8).setCellStyle(styleHeader);
		HSSFDataFormat dataFormat = workbook.createDataFormat();
		HSSFCellStyle styleItem = workbook.createCellStyle();
		styleItem.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		styleItem.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleItem.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleItem.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		styleItem.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleItem.setFont(getFont());
		HSSFCellStyle styleItemDate = workbook.createCellStyle();
		styleItemDate.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		styleItemDate.setBorderRight(HSSFCellStyle.BORDER_THIN);
		styleItemDate.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		styleItemDate.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		styleItemDate.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		styleItemDate.setFont(getFont());
		styleItemDate.setDataFormat(dataFormat.getFormat("dd MMM yyyy"));
		int indexRow = 1;
		for (Story story : list) {
			HSSFRow row = sheet.createRow((short) indexRow);
			row.createCell(0).setCellValue(story.getDate());
			row.createCell(1).setCellValue(story.getBusinessLeader().getName());
			row.createCell(2).setCellValue(story.getProject().getName());
			row.createCell(3).setCellValue(story.getResource().getName());
			row.createCell(4).setCellValue(story.getResource().getRole().getName());
			row.createCell(5).setCellValue(story.getNumber());
			row.createCell(6).setCellValue(story.getPoints());
			row.createCell(7).setCellValue(story.getStatus().getName());
			row.createCell(8).setCellValue(story.getComments());
			row.getCell(0).setCellStyle(styleItemDate);
			row.getCell(1).setCellStyle(styleItem);
			row.getCell(2).setCellStyle(styleItem);
			row.getCell(3).setCellStyle(styleItem);
			row.getCell(4).setCellStyle(styleItem);
			row.getCell(5).setCellStyle(styleItem);
			row.getCell(6).setCellStyle(styleItem);
			row.getCell(7).setCellStyle(styleItem);
			row.getCell(8).setCellStyle(styleItem);
			indexRow++;
		}
		autoFormatSheetColumns(sheet, 8);
	}

	private boolean hasDataOnReport(Resource resource, BusinessLeader businessLeader,
			HashMap<String, List<Story>> storiesMap) {
		boolean hasData = false;
		List<Story> list = storiesMap.get(businessLeader.getName());
		for (Story story : list) {
			if (story.getResource().getId().equals(resource.getId())) {
				hasData = true;
				break;
			}
		}
		return hasData;
	}
}

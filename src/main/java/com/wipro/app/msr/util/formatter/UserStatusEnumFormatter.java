package com.wipro.app.msr.util.formatter;

import org.apache.ibatis.type.EnumTypeHandler;

import com.wipro.app.msr.util.constants.UserStatusEnum;

public class UserStatusEnumFormatter extends EnumTypeHandler<UserStatusEnum>{
	
	public UserStatusEnumFormatter() {
		super(UserStatusEnum.class);
	}
	
}
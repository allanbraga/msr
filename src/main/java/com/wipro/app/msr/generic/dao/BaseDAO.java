package com.wipro.app.msr.generic.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wipro.app.msr.util.exceptions.DAOException;

public class BaseDAO<T, P> implements IBaseDAO<T, P> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private SqlSessionFactory sqlSessionFactory;
	
	private SqlSession sqlSession;

	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

	public SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

	public void openSession() {
		sqlSession = sqlSessionFactory.openSession();
	}

	public void closeSession() {
		sqlSession.close();
	}

	public List<T> selectAll(String entity) throws DAOException {
		openSession();
		List<T> list = null;
		try {
			list = sqlSession.selectList(entity + SELECT_ALL);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession();
		}
		return list;
	}

	public T selectById(String entity, P id) throws DAOException {
		openSession();
		T object = null;
		try {
			object = sqlSession.selectOne(entity + SELECT_BY_ID, id);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession();
		}
		return object;
	}

	public List<T> selectBy(String entity, T object) throws DAOException {
		openSession();
		List<T> list = null;
		try {
			list = sqlSession.selectList(entity + SELECT_BY, object);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession();
		}
		return list;
	}

	public List<T> selectList(String queryId) throws DAOException {
		return selectList(queryId, null);
	}

	public List<T> selectList(String queryId, Object parameter) throws DAOException {
		openSession();
		List<T> list = null;
		try {
			list = sqlSession.selectList(queryId, parameter);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession();
		}
		return list;
	}

	public T selectObject(String queryId, Object parameter) throws DAOException {
		openSession();
		T object = null;
		try {
			object = sqlSession.selectOne(queryId, parameter);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession();
		}
		return object;
	}

	public void insert(String entity, T object) throws DAOException {
		openSession();
		try {
			sqlSession.insert(entity + INSERT, object);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			throw new DAOException(e);
		} finally {
			closeSession();
		}
	}
	
	public void insert(String entity, T object, String dataFormato) throws DAOException {
		openSession();
		try {
			sqlSession.insert(entity + INSERT, object);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			throw new DAOException(e);
		} finally {
			closeSession();
		}
	}

	public void update(String entity, T object) throws DAOException {
		openSession();
		try {
			sqlSession.update(entity + UPDATE, object);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			throw new DAOException(e);
		} finally {
			closeSession();
		}
	}
	
	public void update(String entity, T object, String dataFormato) throws DAOException {
		openSession();
		try {
			sqlSession.update(entity + UPDATE, object);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			throw new DAOException(e);
		} finally {
			closeSession();
		}
	}

	public void delete(String entity, P codigo) throws DAOException {
		openSession();
		try {
			sqlSession.delete(entity + DELETE, codigo);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			throw new DAOException(e);
		} finally {
			closeSession();
		}
	}

	public void insertObject(String queryId, Object parameter) throws DAOException {
		openSession();
		try {
			sqlSession.insert(queryId, parameter);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			throw new DAOException(e);
		} finally {
			closeSession();
		}
	}

	public void updateObject(String queryId, Object parameter) throws DAOException {
		openSession();
		try {
			sqlSession.update(queryId, parameter);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			throw new DAOException(e);
		} finally {
			closeSession();
		}
	}

	public void deleteObject(String queryId, Object parameter) throws DAOException {
		openSession();
		try {
			sqlSession.delete(queryId, parameter);
			sqlSession.commit();
		} catch (Exception e) {
			sqlSession.rollback();
			throw new DAOException(e);
		} finally {
			closeSession();
		}
	}


}

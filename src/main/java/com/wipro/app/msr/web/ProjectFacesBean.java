package com.wipro.app.msr.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.wipro.app.msr.controller.ControllerFacade;
import com.wipro.app.msr.generic.web.BaseFacesBean;
import com.wipro.app.msr.project.Project;
import com.wipro.app.msr.sow.Sow;
import com.wipro.app.msr.util.constants.Messages;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller
@Scope("session")
@ManagedBean(name = "projectFacesBean")
public class ProjectFacesBean extends BaseFacesBean<Project> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1443064834350590375L;

	@Autowired
	private ControllerFacade facade;
	
	private List<SelectItem> sowList;
	

	public ProjectFacesBean() {
		super("project");
		item = new Project();
	}

	public void clean() {
		try {
			list = facade.getProjectController().selectAll();
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String save() {
		try {
			if (item.getId() == null) {
				facade.getProjectController().insert(item);
			} else {
				facade.getProjectController().update(item);
			}
			addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_SUCCESS, "Project");
			item = new Project();
			clean();
			return getListPath();
		} catch (ControllerException e) {
			addWebMessage(e);
		}

		return null;
	}

	public String add() {
		action = "Add";
		item = new Project();
		return getFormPath();
	}

	public String edit(Project row) {
		action = "Edit";
		item = row;
		return getFormPath();
	}

	public String cancel() {
		action = "";
		item = new Project();
		return getListPath();
	}
	
	public void delete(Project row) {		
		try {				
			facade.getProjectController().delete(row.getId());				
			addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_REMOVE_SUCCESS, "Project");	
			clean();
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		
	}
	
	public List<SelectItem> listSow() {
		try {
			this.sowList = new ArrayList<SelectItem>();
			this.sowList.add(new SelectItem(null,"..."));
			List<Sow> sowList = facade.getSowController().selectAll();
			for (Sow sow : sowList) {
				this.sowList.add(new SelectItem(sow, sow.getName()));
			}
			return this.sowList;
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		return null;
	}
	
	
	public Converter getSowConverter() {
		return getEntityConverter(Sow.class, "sow");
	}
	
}

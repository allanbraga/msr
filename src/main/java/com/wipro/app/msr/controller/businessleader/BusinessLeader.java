package com.wipro.app.msr.controller.businessleader;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.wipro.app.msr.controller.resource.Resource;
import com.wipro.app.msr.sow.Sow;
import com.wipro.app.msr.util.constants.UserStatusEnum;
import com.wipro.app.msr.util.interfaces.IPrimaryKey;

public class BusinessLeader implements IPrimaryKey<Integer> {
    
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private String name;
    private UserStatusEnum status;    
    private List<Resource> resources = new ArrayList<>();
    private Sow sow;

    public BusinessLeader() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserStatusEnum getStatus() {
        return status;
    }

    public void setStatus(UserStatusEnum status) {
        this.status = status;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }
    
    
    
    public Sow getSow() {
		return sow;
	}

	public void setSow(Sow sow) {
		this.sow = sow;
	}

	@Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BusinessLeader other = (BusinessLeader) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "BusinessLeader{" + "id=" + id + ", name=" + name + ", status=" + status + ", resources=" + resources + '}';
    }

	@Override
	public Integer getPrimaryKey() {
		return id;
	}

}

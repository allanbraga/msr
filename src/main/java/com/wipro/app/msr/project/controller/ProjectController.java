package com.wipro.app.msr.project.controller;

import org.springframework.stereotype.Controller;

import com.wipro.app.msr.generic.controller.BaseController;
import com.wipro.app.msr.project.Project;

@Controller("projectController")
public class ProjectController extends BaseController<Project, Integer> implements IProjectController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6059272179005516105L;
	
	public ProjectController() {
		super(Project.class);
	}

	

	
	
	

}

package com.wipro.app.msr.util.constants;

public abstract class Messages {
	
	
	public static final String DEFAULT_MESSAGE = "generic.error";
	public static final String INVALID_FIELD = "invalid.field";
	public static final String DOWNLOAD_ERROR = "download.error";
	public static final String ITEM_SUCCESS = "item.save.success";
	public static final String ITEM_REMOVE_SUCCESS = "item.remove.success";
	public static final String DUPLICATED_ITEM = "duplicated.item";
	public static final String DUPLICATED_RESOURCE = "duplicated.resource";
	public static final String STORIES_ASSIGNED = "stories.assigned";
	
}
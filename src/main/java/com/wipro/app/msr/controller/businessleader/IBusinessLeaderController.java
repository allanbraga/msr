package com.wipro.app.msr.controller.businessleader;

import com.wipro.app.msr.generic.controller.IBaseController;

public interface IBusinessLeaderController extends IBaseController<BusinessLeader, Integer> {
	
}

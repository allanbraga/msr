package com.wipro.app.msr.controller.login;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import com.wipro.app.msr.user.User;
import com.wipro.app.msr.user.controller.UserController;
import com.wipro.app.msr.util.PasswordEncoderGenerator;
import com.wipro.app.msr.util.exceptions.ControllerException;

@SessionScoped
@ManagedBean(name="loginController")
public class LoginController {
	
	private static final Log LOG = LogFactory.getLog(LoginController.class);
	
	@ManagedProperty(value="#{authenticationManager}")
	private AuthenticationManager authenticationManager;
	
	@ManagedProperty(value="#{userController}")	
	private UserController userController;
	
	private User user = new User();
	
	private String newPassword;
	
	private String confirmPassword;
	
	
	/**
	 * the login action called by the view
	 * 
	 * @return
	 */
	public String login() {
		try {
			
			// check if userdata is given
			if (user.getLogin() == null || user.getPassword() == null) {
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "login.failed");
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				LOG.info("Login not started because userName or Password is empty: " + user.getLogin());
				return null;
			}

			// authenticate afainst spring security
			Authentication  request = new UsernamePasswordAuthenticationToken(user.getLogin(),
					user.getPassword());

			Authentication result = authenticationManager.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);
			populateUser(result);
			
		} catch (AuthenticationException e) {
			LOG.info("Login failed: " + e.getMessage());
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "login.failed");
			FacesContext.getCurrentInstance().addMessage(null, facesMsg);

			return null;
		}
		return "/pages/home.xhtml";

	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	private void populateUser(Authentication result){
		try {
			user = userController.getUserByLogin(result.getName());
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String changePassword(){			
		try {
			user.setPassword(PasswordEncoderGenerator.encodePassword(newPassword));
			userController.update(user);
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,"","Password changed.");
			FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			this.newPassword = "";
			this.confirmPassword = "";
		} catch (ControllerException e) {
			LOG.info("changePassword failed: " + e.getMessage());
			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "changePassword failed");
			FacesContext.getCurrentInstance().addMessage(null, facesMsg);
		}
		
		return "";
	}
	
	public String cancel(){
		
		return "/pages/home.xhtml";
	}
	
	public String logout() {
		user = null;
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();        
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
        	
			context.redirect(context.getRequestContextPath() + "/j_spring_security_logout");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return null;
    }

	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}

	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	public UserController getUserController() {
		return userController;
	}

	public void setUserController(UserController userController) {
		this.userController = userController;
	}
	
	
	
	
}

package com.wipro.app.msr.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.wipro.app.msr.controller.ControllerFacade;
import com.wipro.app.msr.controller.businessleader.BusinessLeader;
import com.wipro.app.msr.generic.web.BaseFacesBean;
import com.wipro.app.msr.sow.Sow;
import com.wipro.app.msr.util.constants.UserStatusEnum;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller
@Scope("session")
@ManagedBean(name = "applicationFacesBean")
public class ApplicationFacesBean extends BaseFacesBean<BusinessLeader> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ControllerFacade facade;
	
	@Autowired
	private ResourceFacesBean resourceFacesBean;
	
	@Autowired
	private StoryFacesBean storyFacesBean;
	
	@Autowired
	private UserFacesBean userFacesBean;
	
	@Autowired
	private ProjectFacesBean projectFacesBean;
	
	@Autowired
	private SowFacesBean sowFacesBean;

	private Integer indexSelected;
	
	private List<Sow> sowList;	
	
	private Sow sow;

	public ApplicationFacesBean() {
		super("home");
		indexSelected = 0;
	}
	

	public String goToHome() {
		indexSelected = 0;
		return getListPath();
	}

	public String goToBusinessLeader() {
		indexSelected = 1;
		return getOtherPath("business-leader");
	}

	public String goToResource() {
		indexSelected = 2;
		resourceFacesBean.clean();
		return getOtherPath("resource");
	}

	public String goToStory() {
		indexSelected = 3;
		storyFacesBean.clean();
		return getOtherPath("story");
	}

	public String goToReport() {
		indexSelected = 4;
		return getOtherPath("report");
	}
	
	public String goToUser() {
		indexSelected = 5;
		userFacesBean.clean();
		return getOtherPath("user");
	}
	
	public String goToProject() {
		indexSelected = 6;
		projectFacesBean.clean();
		return getOtherPath("project");
	}
	
	public String goToSow() {
		indexSelected = 7;
		sowFacesBean.clean();
		return getOtherPath("sow");
	}

	public Integer getIndexSelected() {
		return indexSelected;
	}
		
	
	public List<Sow> getSowList() {
		return sowList;
	}

	public void setSowList(List<Sow> sowList) {
		this.sowList = sowList;
	}
	

	public Sow getSow() {
		return sow;
	}

	public void setSow(Sow sow) {
		this.sow = sow;
	}

	public List<Sow> listSow() {
		try {
			this.sowList = new ArrayList<Sow>();			
			this.sowList = facade.getSowController().selectAll();			
			return this.sowList;
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		return null;
	}
	
	public void listBusinessLeader(Sow sowParam){		
		try {
			BusinessLeader filter = new BusinessLeader();
			filter.setStatus(UserStatusEnum.ACTIVE);
			filter.setSow(sowParam);
			this.sow = sowParam;
			this.list = new ArrayList<>();			
			this.setList(facade.getBusinessLeaderController().selectBy(filter));
		} catch (ControllerException e) {
			e.printStackTrace();
		}
		
	}

}

package com.wipro.app.msr.generic.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.wipro.app.msr.generic.dao.IBaseDAO;
import com.wipro.app.msr.util.exceptions.ControllerException;
import com.wipro.app.msr.util.interfaces.IPrimaryKey;

public abstract class BaseController<T extends IPrimaryKey<PK>, PK extends Serializable> implements IBaseController<T, PK> {
	
	private static final long serialVersionUID = 1L;

	private Class<T> classType;
	
	@Autowired
	private IBaseDAO<T,PK> dao;

	public BaseController(Class<T> classType) {
		this.classType = classType;
	}

	public IBaseDAO<T,PK> getDAO() {
		return dao;
	}

	public void setDAO(IBaseDAO<T,PK> dao) {
		this.dao = dao;
	}

	protected String getEntity() {
		return classType.getSimpleName();
	}

	public T selectById(PK id) throws ControllerException {
		return dao.selectById(getEntity(), id);
	}

	public List<T> selectAll() throws ControllerException {
		return dao.selectAll(getEntity());
	}
	
	public List<T> selectBy(T object) throws ControllerException {
		return dao.selectBy(getEntity(), object);
	}

	public List<T> selectList(String queryId) throws ControllerException {
		return selectList(queryId, null);
	}

	public List<T> selectList(String queryId, Object parameter) throws ControllerException {
		return dao.selectList(getEntity() + "." + queryId, parameter);
	}
	
	public List<T> selectList(String queryId, Map<String, Object> parameter) throws ControllerException {
		return dao.selectList(getEntity() + "." + queryId, parameter);
	}
		
	public T selectObject(String queryId, Object parameter) throws ControllerException {
		return dao.selectObject(getEntity() + "." + queryId, parameter);
	}

	public void insert(T object) throws ControllerException {
		dao.insert(getEntity(), object);
	}
	

	public void update(T object) throws ControllerException {
		dao.update(getEntity(), object);
	}
	
	public void delete(PK id) throws ControllerException {
		dao.delete(getEntity(), id);
	}
}
package com.wipro.app.msr.util.constants;

public enum RoleEnum {
 
	NONE("..."), DEVELOPER("Developer"), TESTER("Tester"), BUSINESS_ANALYST("Business Analyst") , SET("SET");
    
    private final String name;
    
    private RoleEnum(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public String toString() {
        return name;
    }
    
    public static RoleEnum getByName(String name) {
        for (RoleEnum role : RoleEnum.values()) {
            if (role.getName().equals(name)) {
                return role;
            }
        }
        return null;
    } 
    
}

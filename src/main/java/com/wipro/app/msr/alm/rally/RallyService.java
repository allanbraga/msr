package com.wipro.app.msr.alm.rally;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.JsonObject;
import com.rallydev.rest.RallyRestApi;
import com.rallydev.rest.request.QueryRequest;
import com.rallydev.rest.response.QueryResponse;
import com.rallydev.rest.util.Fetch;
import com.rallydev.rest.util.QueryFilter;
import com.wipro.app.msr.alm.ALMFilter;
import com.wipro.app.msr.alm.ALMService;
import com.wipro.app.msr.alm.ALMStory;
import com.wipro.app.msr.util.constants.TicketStatusEnum;

public class RallyService implements ALMService {

	private RallyRestApi rallyRestApi;

	public RallyService(String serviceUrl, String apiKey) {
		try {
			rallyRestApi = new RallyRestApi(new URI(serviceUrl), apiKey);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<ALMStory> listStoriesByOwner(ALMFilter almFilter) {
		List<ALMStory> rallyStories = null;
		QueryResponse response;
		try {
			response = rallyRestApi.query(getStoryQueryFilter(almFilter));
			rallyStories = new ArrayList<ALMStory>();
			ALMStory story;
			if (response.getResults().size() > 0) {
				for (int i = 0; i < response.getResults().size(); i++) {
					story = convertStory(response.getResults().get(i).getAsJsonObject());
					if (!storyContains(story.getNumber(), rallyStories)) {
						rallyStories.add(story);
					}

				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rallyStories;
	}

	@Override
	public List<ALMStory> listStoriesByConversationPost(ALMFilter almFilter) {
		QueryResponse response;
		List<ALMStory> rallyStories = null;
		try {
			response = rallyRestApi.query(getConversationQueryFilter(almFilter));
			rallyStories = new ArrayList<ALMStory>();
			ALMStory story;
			if (response.getResults().size() > 0) {
				for (int i = 0; i < response.getResults().size(); i++) {
					story = convertConversationPost(response.getResults().get(i).getAsJsonObject());
					if (story.getNumber() != null && !storyContains(story.getNumber(), rallyStories)) {
						rallyStories.add(story);
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rallyStories;

	}
	
	@Override
	public List<ALMStory> listStoriesByTestCaseResult(ALMFilter almFilter) {
		
		QueryResponse response;
		List<ALMStory> rallyStories = null;
		try {
			response = rallyRestApi.query(getTestCaseResultQueryFilter(almFilter));
			rallyStories = new ArrayList<ALMStory>();
			ALMStory story;
			if (response.getResults().size() > 0) {
				for (int i = 0; i < response.getResults().size(); i++) {
					story = convertTestCaseResult(response.getResults().get(i).getAsJsonObject());
					if (story != null && story.getNumber() != null && !storyContains(story.getNumber(), rallyStories)) {
						rallyStories.add(story);
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rallyStories;
	}

	private QueryRequest getConversationQueryFilter(ALMFilter almFilter) {
		QueryRequest request = new QueryRequest(RallyEntity.CONVERSATION_POST.requestType);
		request.setFetch(new Fetch("FormattedID", "CreationDate", "_refObjectName", "Artifact", "InProgressDate",
				"PlanEstimate", "ScheduleStatePrefix", "ScheduleState","Estimate","State"));
		request.setQueryFilter(new QueryFilter("User", "=", almFilter.getUserId())
				.and(new QueryFilter("CreationDate", ">=", formatDate(almFilter.getStartDate())))
				.and(new QueryFilter("CreationDate", "<=", formatDate(almFilter.getEndDate()))));
		request.setOrder("CreationDate asc");
		return request;
	}

	private QueryRequest getStoryQueryFilter(ALMFilter almFilter) {
		QueryRequest request = new QueryRequest(RallyEntity.STORY.requestType);
		request.setFetch(new Fetch("FormattedID", "PlanEstimate", "_refObjectName", "InProgressDate",
				"ConversationPost", "ScheduleState", "ScheduleStatePrefix"));
		request.setQueryFilter(new QueryFilter("Owner", "=", almFilter.getUserId())
				.and(new QueryFilter("InProgressDate", ">=", formatDate(almFilter.getStartDate())))
				.and(new QueryFilter("InProgressDate", "<=", formatDate(almFilter.getEndDate()))));
		request.setOrder("InProgressDate asc");
		return request;
	}
	
	private QueryRequest getTestCaseResultQueryFilter(ALMFilter almFilter) {
		QueryRequest request = new QueryRequest(RallyEntity.TEST_CASE_RESULT.requestType);
		request.setFetch(new Fetch("FormattedID", "CreationDate", "_refObjectName", "Tester","TestCase","Date","TestFolder","WorkProduct"));
		request.setQueryFilter(new QueryFilter("Tester", "=", almFilter.getUserId())
				.and(new QueryFilter("CreationDate", ">=", formatDate(almFilter.getStartDate())))
				.and(new QueryFilter("CreationDate", "<=", formatDate(almFilter.getEndDate()))));
		request.setOrder("CreationDate asc");
		return request;
	}

	private String formatDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(date);
	}

	private Date formatDate(String date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return dateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private TicketStatusEnum getStatus(String status) {
		TicketStatusEnum ticketStatusEnum = TicketStatusEnum.NONE;

		switch (status) {
		case "A":
			ticketStatusEnum = TicketStatusEnum.COMPLETED;
			break;
		case "C":
			ticketStatusEnum = TicketStatusEnum.PROGRESS;
			break;
		case "R":
			ticketStatusEnum = TicketStatusEnum.COMPLETED;
			break;
		case "P":
			ticketStatusEnum = TicketStatusEnum.PROGRESS;
			break;
		case "U":
			ticketStatusEnum = TicketStatusEnum.PROGRESS;
			break;
		case "D":
			ticketStatusEnum = TicketStatusEnum.PROGRESS;
			break;
		default:
			break;
		}

		return ticketStatusEnum;
	}
	
	private TicketStatusEnum getTaskStatus(String status) {
		TicketStatusEnum ticketStatusEnum = TicketStatusEnum.NONE;

		switch (status) {
		case "Completed":
			ticketStatusEnum = TicketStatusEnum.COMPLETED;
			break;		
		default:
			break;
		}

		return ticketStatusEnum;
	}

	private ALMStory convertStory(JsonObject jsonObject) {
		ALMStory story = new ALMStory();
		story.setPoints(jsonObject.get("PlanEstimate").isJsonNull() ? null : jsonObject.get("PlanEstimate").getAsInt());
		story.setNumber(jsonObject.get("FormattedID").getAsString());
		story.setDate(formatDate(jsonObject.get("InProgressDate").getAsString()));
		story.setStatus(getStatus(jsonObject.get("ScheduleStatePrefix").getAsString()));
		story.setDescription(jsonObject.get("_refObjectName").getAsString());
		story.setAlmStatus(jsonObject.get("ScheduleState").getAsString());

		return story;
	}

	private ALMStory convertConversationPost(JsonObject jsonObject) {

		ALMStory story = new ALMStory();
		String type = jsonObject.get("Artifact").getAsJsonObject().get("_type").getAsString();
		if ("HierarchicalRequirement".equals(type) || "Defect".equals(type)) {
			JsonObject artifact = jsonObject.get("Artifact").getAsJsonObject();		
			
			if (!artifact.get("PlanEstimate").isJsonNull()) {
				story.setPoints(artifact.get("PlanEstimate").getAsInt());
			} else {
				story.setPoints(0);
			}
			story.setNumber(artifact.get("FormattedID").getAsString());
			if (!jsonObject.get("CreationDate").isJsonNull()) {
				story.setDate(formatDate(jsonObject.get("CreationDate").getAsString()));
			}			
			story.setStatus(getStatus(artifact.get("ScheduleStatePrefix").getAsString()));
			story.setDescription(artifact.get("_refObjectName").getAsString());
			if (artifact.get("ScheduleState") != null) {
				story.setAlmStatus(artifact.get("ScheduleState").getAsString());
			}			
		}
		
		return story;
	}

	private ALMStory convertTestCaseResult(JsonObject jsonObject) {
		ALMStory story =  null;
		// getting testcases just it is not related to any story or defect
		if(!jsonObject.get("TestCase").isJsonNull() && jsonObject.get("TestCase").getAsJsonObject().get("WorkProduct").isJsonNull()){
			story = new ALMStory();
			story.setPoints(0);
			story.setNumber(jsonObject.get("TestCase").getAsJsonObject().get("FormattedID").getAsString());
			story.setDate(formatDate(jsonObject.get("CreationDate").getAsString()));
			story.setStatus(TicketStatusEnum.COMPLETED);
			
		}		
		return story;
	}

	private boolean storyContains(String storyNumber, List<ALMStory> stories) {

		boolean contains = false;
		if (stories != null) {
			for (ALMStory story : stories) {
				if (storyNumber.equals(story.getNumber())) {
					contains = true;
					break;
				}
			}
		}
		return contains;

	}

	

}

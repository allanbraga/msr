package com.wipro.app.msr.alm.rally;

public enum RallyEntity {
	
	TEST_FOLDER ("Test Folder", "testfolder"),
    TEST_CASE("Test Case", "testcase"),
    TEST_SET("Test Set", "testset"),
    STORY("Story","hierarchicalrequirement"),
    CONVERSATION_POST("Conversation Post","conversationpost"),
    TEST_CASE_RESULT("Test Case Result","testcaseresult");

    final String name;
    final String requestType;

    RallyEntity(String name, String requestType) {
        this.name = name;
        this.requestType = requestType;
    }

}

package com.wipro.app.msr.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.wipro.app.msr.controller.ControllerFacade;
import com.wipro.app.msr.controller.businessleader.BusinessLeader;
import com.wipro.app.msr.controller.resource.Resource;
import com.wipro.app.msr.controller.story.Story;
import com.wipro.app.msr.generic.web.BaseFacesBean;
import com.wipro.app.msr.project.Project;
import com.wipro.app.msr.util.constants.Messages;
import com.wipro.app.msr.util.constants.RoleEnum;
import com.wipro.app.msr.util.constants.UserStatusEnum;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller
@Scope("session")
@ManagedBean(name = "resourceFacesBean")
public class ResourceFacesBean extends BaseFacesBean<Resource> {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ControllerFacade facade;

	private List<SelectItem> status;
	private List<SelectItem> businessLeaders;
	private List<SelectItem> roles;
	private List<SelectItem> projects;

	public ResourceFacesBean() {
		super("resource");
		item = new Resource();
		filter = new Resource();
	}

	public void filter() {
		try {
			if (filter.getRole() == RoleEnum.NONE) {
				filter.setRole(null);
			}
			if (filter.getStatus() == UserStatusEnum.NONE) {
				filter.setStatus(null);
			}
			list = facade.getResourceController().selectBy(filter);
		} catch (ControllerException e) {
			e.printStackTrace();
		}
	}
	
	public void clean() {
		filter = new Resource();
		filter();
	}

	public List<SelectItem> listStatus() {
		this.status = new ArrayList<SelectItem>();
		for (UserStatusEnum status : UserStatusEnum.values()) {
			this.status.add(new SelectItem(status, status.toString()));
		}
		return this.status;
	}

	public List<SelectItem> listRoles() {
		this.roles = new ArrayList<SelectItem>();
		for (RoleEnum role : RoleEnum.values()) {
			this.roles.add(new SelectItem(role, role.toString()));
		}
		return this.roles;
	}

	public List<SelectItem> listBusinessLeaders() {
		try {
			this.businessLeaders = new ArrayList<SelectItem>();
			this.businessLeaders.add(new SelectItem(null,"..."));
			List<BusinessLeader> businessLeadersList = facade.getBusinessLeaderController().selectAll();
			for (BusinessLeader businessLeader : businessLeadersList) {
				this.businessLeaders.add(new SelectItem(businessLeader, businessLeader.getName()));
			}
			return this.businessLeaders;
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		return null;
	}

	public String add() {
		action = "Add";
		item = new Resource();
		return getFormPath();
	}

	public String edit(Resource row) {
		action = "Edit";
		item = row;
		return getFormPath();
	}

	public String cancel() {
		action = "";
		item = new Resource();
		return getListPath();
	}

	public String save() {
		if (validate()) {
			try {
				if (item.getId() == null) {
					facade.getResourceController().insert(item);
				} else {
					facade.getResourceController().update(item);
				}
				addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_SUCCESS, "Resource");
				item = new Resource();
				filter();
				return getListPath();
			} catch (ControllerException e) {
				addWebMessage(e);
			}
		}
		return null;
	}
	
	public void delete(Resource row) {
		if (validateDeleteResource(row)) {
			try {				
				facade.getResourceController().delete(row.getId());				
				addMessage(FacesMessage.SEVERITY_INFO, Messages.ITEM_REMOVE_SUCCESS, "Resource");
				item = new Resource();
				filter();				
			} catch (ControllerException e) {
				addWebMessage(e);
			}
		}
	}
	

	private boolean validate() {
		boolean valid = true;
		if (item.getName() == null || item.getName().trim().equals("")) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Name");
			valid = false;
		}
		if (item.getStatus() == null || item.getStatus() == UserStatusEnum.NONE) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Status");
			valid = false;
		}
		if (item.getRole() == null || item.getRole() == RoleEnum.NONE) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Role");
			valid = false;
		}
		if (item.getBusinessLeader() == null) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "Business Leader");
			valid = false;
		}
		if (item.getEmployeeId() == null || item.getEmployeeId().trim().equals("")) {
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.INVALID_FIELD, "EmployeeID");
			valid = false;
		}		
		if(!validateUserExists()){			
			addMessage(FacesMessage.SEVERITY_ERROR, Messages.DUPLICATED_RESOURCE, item.getName());
			valid = false;
		}		
		
		return valid;
	}

	public Converter getBusinessLeaderConverter() {
		return getEntityConverter(BusinessLeader.class, "business-leader");
	}
	
	private boolean validateDeleteResource(Resource resource){
		boolean valid = true;
		Story storyFilter = new Story();
		storyFilter.setResource(resource);
		try {
			List<Story> stories = facade.getStoryController().selectBy(storyFilter);
			if(!stories.isEmpty()){
				addMessage(FacesMessage.SEVERITY_ERROR, Messages.STORIES_ASSIGNED);
				valid = false;
			}
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return valid;
	}
	
	private boolean validateUserExists(){		
		boolean valid = true;
		Resource newResource = new Resource();
		newResource.setName(item.getName());
		newResource.setBusinessLeader(item.getBusinessLeader());		
		try {
			List<Resource> resourceList = facade.getResourceController().selectBy(newResource);
			if(!resourceList.isEmpty()){
				for (Resource resource : resourceList) {
					if(resource.getName().equals(newResource.getName()) && !resource.getId().equals(item.getId())){
						valid = false;
						break;
					}					
					if(resource.getEmployeeId() != null && resource.getEmployeeId().equals(newResource.getEmployeeId()) && !resource.getId().equals(item.getId())){
						valid = false;
						break;
					}
				}
			}
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return valid;
		
	}
	
	public List<SelectItem> listProject() {
		try {
			this.projects = new ArrayList<SelectItem>();
			this.projects.add(new SelectItem(null,"..."));
			List<Project> projectList = facade.getProjectController().selectAll();
			for (Project project : projectList) {
				this.projects.add(new SelectItem(project, project.getName()));
			}
			return this.projects;
		} catch (ControllerException e) {
			addWebMessage(e);
		}
		return null;
	}
	
	public Converter getProjectConverter() {
		return getEntityConverter(Project.class, "project");
	}

}

package com.wipro.app.msr.util.constants;

public enum UserStatusEnum {
 
    NONE("..."), ACTIVE("Active"), INACTIVE("Inactive");
    
    private final String name;
    
    private UserStatusEnum(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    public static UserStatusEnum getByName(String name) {
        for (UserStatusEnum status : UserStatusEnum.values()) {
            if (status.getName().equals(name)) {
                return status;
            }
        }
        return null;
    } 
    
}

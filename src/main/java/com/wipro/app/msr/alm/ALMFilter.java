package com.wipro.app.msr.alm;

import java.io.Serializable;
import java.util.Date;

public class ALMFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2716174406716890637L;

	private String userId;
	private Date startDate;
	private Date endDate;
	private FilterTypeEnum filterTypeEnum;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public FilterTypeEnum getFilterTypeEnum() {
		return filterTypeEnum;
	}

	public void setFilterTypeEnum(FilterTypeEnum filterTypeEnum) {
		this.filterTypeEnum = filterTypeEnum;
	}


	public static void main(String[] args) {


			String json = "{\n" +
					"  \"id\" : 22,\n" +
					"  \"products\" : [ ],\n" +
					"  \"_links\" : {\n" +
					"    \"self\" : {\n" +
					"      \"href\" : \"https://cart-service-di20026792.cfapps.io/carts/22\"\n" +
					"    },\n" +
					"    \"cart\" : {\n" +
					"      \"href\" : \"https://cart-service-di20026792.cfapps.io/carts/22\"\n" +
					"    }\n" +
					"  }\n" +
					"}";


			String test = json.replaceAll(" :",":").replaceAll(": ",":");

			System.out.println(test);


			System.out.print(test.contains("\"id\":22"));




	}
	

}

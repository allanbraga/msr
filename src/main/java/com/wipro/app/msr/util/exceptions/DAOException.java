package com.wipro.app.msr.util.exceptions;

public class DAOException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private Object[] parameters;

	public DAOException(String message) {
		super(message);
	}

	public DAOException(String message, Object[] parameters) {
		super(message);
		this.parameters = parameters;
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(Throwable exception) {
		super(exception);
	}

	public Object[] getParameters() {
		return parameters;
	}
}
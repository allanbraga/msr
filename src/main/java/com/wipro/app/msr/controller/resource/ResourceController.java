package com.wipro.app.msr.controller.resource;

import java.util.List;

import org.springframework.stereotype.Controller;

import com.wipro.app.msr.generic.controller.BaseController;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller
public class ResourceController extends BaseController<Resource, Integer> implements IResourceController {
	
	private static final long serialVersionUID = 1L;

	public ResourceController() {
		super(Resource.class);
	}
	
	
	@Override
	public List<Resource> getResourceByBusinessLeader(Integer businessLeaderId) throws ControllerException {
		return selectList("select-by-business-leader", businessLeaderId);
	}

	@Override
	public Resource getResourceByName(String name) throws ControllerException {
		return selectObject("select-by-name", name);
	}

}

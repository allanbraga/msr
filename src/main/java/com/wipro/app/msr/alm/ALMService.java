package com.wipro.app.msr.alm;

import java.util.List;

public interface ALMService {
	
	List<ALMStory> listStoriesByOwner(ALMFilter almFilter);
	List<ALMStory> listStoriesByConversationPost(ALMFilter almFilter);
	List<ALMStory> listStoriesByTestCaseResult(ALMFilter almFilter);

}

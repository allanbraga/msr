package com.wipro.app.msr.controller.story;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.wipro.app.msr.alm.ALMFilter;
import com.wipro.app.msr.alm.ALMService;
import com.wipro.app.msr.alm.ALMStory;
import com.wipro.app.msr.alm.FilterTypeEnum;
import com.wipro.app.msr.generic.controller.BaseController;
import com.wipro.app.msr.util.exceptions.ControllerException;

@Controller
public class StoryController extends BaseController<Story, Integer> implements IStoryController {
	
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private ALMService almService;

	public StoryController() {
		super(Story.class);
	}
	
	public List<Story> getStoriesByDateRange(Date startDate, Date endDate , Integer sowId) throws ControllerException {
		HashMap<String, Object> parameters = new HashMap<>();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		parameters.put("startDate", simpleDateFormat.format(startDate));
		parameters.put("endDate", simpleDateFormat.format(endDate));
		parameters.put("sowId", sowId);
		return selectList("select-by-date-range", parameters);
	}

	@Override
	public List<Story> getStoriesCountByDateRangeForBusinessLeader(Date startDate, Date endDate , Integer sowId) throws ControllerException {
		HashMap<String, Object> parameters = new HashMap<>();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		parameters.put("startDate", simpleDateFormat.format(startDate));
		parameters.put("endDate", simpleDateFormat.format(endDate));
		parameters.put("sowId", sowId);
		return selectList("select-count-stories", parameters);
	}

	@Override
	public List<Story> getStoriesPointsByDateRangeForBusinessLeader(Date startDate, Date endDate , Integer sowId) throws ControllerException {
		HashMap<String, Object> parameters = new HashMap<>();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		parameters.put("startDate", simpleDateFormat.format(startDate));
		parameters.put("endDate", simpleDateFormat.format(endDate));
		parameters.put("sowId", sowId);
		return selectList("select-count-points", parameters);
	}

	@Override
	public List<Story> getStoriesCompletedPointsByDateRangeForBusinessLeader(Date startDate, Date endDate , Integer sowId) throws ControllerException {
		HashMap<String, Object> parameters = new HashMap<>();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		parameters.put("startDate", simpleDateFormat.format(startDate));
		parameters.put("endDate", simpleDateFormat.format(endDate));
		parameters.put("sowId", sowId);
		return selectList("select-count-completed-points", parameters);
	}

	@Override
	public List<ALMStory> getALMStories(ALMFilter almFilter) throws ControllerException {
		if(FilterTypeEnum.OWNER_ID.equals(almFilter.getFilterTypeEnum())){		
			return almService.listStoriesByOwner(almFilter);
		}else{
			return almService.listStoriesByConversationPost(almFilter);
		}		
	}
	
}

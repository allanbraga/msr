package com.wipro.app.msr.report;

public class ProjectInformation {

	private String projectName;
	private Integer totalPoints;
	private Integer totalStories;
	private Integer totalCompleted;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(Integer totalPoints) {
		this.totalPoints = totalPoints;
	}

	public Integer getTotalStories() {
		return totalStories;
	}

	public void setTotalStories(Integer totalStories) {
		this.totalStories = totalStories;
	}

	public Integer getTotalCompleted() {
		return totalCompleted;
	}

	public void setTotalCompleted(Integer totalCompleted) {
		this.totalCompleted = totalCompleted;
	}

}

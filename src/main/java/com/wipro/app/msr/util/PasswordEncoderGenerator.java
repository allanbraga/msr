package com.wipro.app.msr.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoderGenerator {
	
	
	public static String encodePassword(String password){
		String cryptedPassword = new BCryptPasswordEncoder().encode(password);		
		return cryptedPassword;		
	}
		

}

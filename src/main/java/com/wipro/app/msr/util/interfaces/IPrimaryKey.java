package com.wipro.app.msr.util.interfaces;

import java.io.Serializable;

public interface IPrimaryKey<T> extends Serializable {

	public T getPrimaryKey();

}

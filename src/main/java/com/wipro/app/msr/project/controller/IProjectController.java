package com.wipro.app.msr.project.controller;

import com.wipro.app.msr.generic.controller.IBaseController;
import com.wipro.app.msr.project.Project;

public interface IProjectController extends IBaseController<Project, Integer> {

}

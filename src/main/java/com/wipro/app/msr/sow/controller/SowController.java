package com.wipro.app.msr.sow.controller;

import org.springframework.stereotype.Controller;

import com.wipro.app.msr.generic.controller.BaseController;
import com.wipro.app.msr.sow.Sow;

@Controller("sowController")
public class SowController extends BaseController<Sow, Integer> implements ISowController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7210707847013920376L;
	
	public SowController() {
		super(Sow.class);
	}	
	

}

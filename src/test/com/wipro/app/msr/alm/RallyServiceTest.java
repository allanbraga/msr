package com.wipro.app.msr.alm;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import com.rallydev.rest.RallyRestApi;
import com.rallydev.rest.request.QueryRequest;
import com.rallydev.rest.response.QueryResponse;
import com.rallydev.rest.util.Fetch;
import com.rallydev.rest.util.QueryFilter;

public class RallyServiceTest {

	private static final String RALLY_KEY = "_n1tf4lZBRHuILXwySZi0fP4kpLBWcETakRHsYtI3JM";

	public static void main(String[] args) throws URISyntaxException, IOException {

		listConversationPostByUser();
	}

	public static void listStoryByUser() throws URISyntaxException, IOException {

		String userId = "e050752@mastercard.com";
		QueryRequest request = new QueryRequest(RallyEntity.STORY.requestType);
		request.setFetch(new Fetch("FormattedID", "PlanEstimate", "_refObjectName", "InProgressDate",
				"ConversationPost", "ScheduleState"));
		request.setQueryFilter(
				new QueryFilter("Owner", "=", userId).and(new QueryFilter("InProgressDate", ">=", "2017-09-25"))
						.and(new QueryFilter("InProgressDate", "<=", "2017-10-25")));
		request.setOrder("InProgressDate asc");
		RallyRestApi restApi = new RallyRestApi(new URI("https://rally1.rallydev.com/"), RALLY_KEY);
		QueryResponse response = restApi.query(request);
		if (response.getResults().size() > 0) {
			System.out.println("TOTAL RECORDS : " + response.getResults().size());

			for (int i = 0; i < response.getResults().size(); i++) {
				System.out.println("STORY : "
						+ response.getResults().get(i).getAsJsonObject().get("FormattedID").getAsString() + " - "
						+ response.getResults().get(i).getAsJsonObject().get("_refObjectName").getAsString()
						+ " Points :" + response.getResults().get(i).getAsJsonObject().get("PlanEstimate") + " Status :"
						+ response.getResults().get(i).getAsJsonObject().get("ScheduleState") + " InProgressDate :"
						+ response.getResults().get(i).getAsJsonObject().get("InProgressDate") + " ConversationPost :"
						+ response.getResults().get(i).getAsJsonObject().get("ConversationPost"));
			}
		}
	}

	public static void listConversationPostByUser() throws IOException, URISyntaxException {

		String userId = "e074467@mastercard.com";
		QueryRequest request = new QueryRequest(RallyEntity.CONVERSATION_POST.requestType);
		request.setFetch(new Fetch("FormattedID", "CreationDate", "_refObjectName", "Artifact", "PlanEstimate",
				"ScheduleStatePrefix", "ScheduleState","Estimate","State"));
		request.setQueryFilter(
				new QueryFilter("User", "=", userId).and(new QueryFilter("CreationDate", ">=", "2017-09-25"))
						.and(new QueryFilter("CreationDate", "<=", "2017-10-25")));
		RallyRestApi restApi = new RallyRestApi(new URI("https://rally1.rallydev.com/"), RALLY_KEY);
		QueryResponse response = restApi.query(request);
		if (response.getResults().size() > 0) {
			System.out.println("TOTAL RECORDS : " + response.getResults().size());

			for (int i = 0; i < response.getResults().size(); i++) {
				System.out.println("TYPE : " + response.getResults().get(i).getAsJsonObject().get("Artifact")
						.getAsJsonObject().get("_type").getAsString());

				if (response.getResults().get(i).getAsJsonObject().get("Artifact").getAsJsonObject().get("_type")
						.getAsString().equals("HierarchicalRequirement")) {
					System.out.println("_ref : "
							+ response.getResults().get(i).getAsJsonObject().get("_ref").getAsString() + " Type : "
							+ response.getResults().get(i).getAsJsonObject().get("Artifact").getAsJsonObject()
									.get("_type").getAsString()
							+ " Date : "
							+ response.getResults().get(i).getAsJsonObject().get("CreationDate").getAsString()
							+ " Story : "
							+ response.getResults().get(i).getAsJsonObject().get("Artifact").getAsJsonObject()
									.get("FormattedID").getAsString()
							+ " - "
							+ response.getResults().get(i).getAsJsonObject().get("Artifact").getAsJsonObject()
									.get("_refObjectName").getAsString()
							+ " Points : " + response.getResults().get(i).getAsJsonObject().get("Artifact")
									.getAsJsonObject().get("PlanEstimate").getAsString());
				} else if (response.getResults().get(i).getAsJsonObject().get("Artifact").getAsJsonObject().get("_type")
						.getAsString().equals("Defect")) {
					System.out.println("TYPE : " + response.getResults().get(i).getAsJsonObject().get("Artifact")
							.getAsJsonObject().get("_type").getAsString());
					
					System.out.println("DATE - CREATE : " + response.getResults().get(i).getAsJsonObject().get("Artifact")
							.getAsJsonObject().get("CreationDate").getAsString());
					
					System.out.println("DATE - CONVERSATION : " + response.getResults().get(i).getAsJsonObject().get("CreationDate").getAsString());
					
				} else if (response.getResults().get(i).getAsJsonObject().get("Artifact").getAsJsonObject().get("_type")
						.getAsString().equals("Task")) {
					System.out.println("TYPE : " + response.getResults().get(i).getAsJsonObject().get("Artifact")
							.getAsJsonObject().get("_type").getAsString());
					System.out.println("Estimate : " + response.getResults().get(i).getAsJsonObject().get("Artifact")
							.getAsJsonObject().get("Estimate").getAsString());
					System.out.println("State" + response.getResults().get(i).getAsJsonObject().get("Artifact")
							.getAsJsonObject().get("State").getAsString());
					System.out.println("InProgressDate : " + response.getResults().get(i).getAsJsonObject().get("Artifact")
							.getAsJsonObject().get("InProgressDate").getAsString());
					
					
				}
			}
		}
	}

	public static void listTestCase() throws IOException, URISyntaxException {

		String userId = "e074466@mastercard.com";
		QueryRequest request = new QueryRequest(RallyEntity.TEST_CASE_RESULT.requestType);
		request.setFetch(new Fetch("FormattedID", "CreationDate", "_refObjectName", "Tester","TestCase","Date","TestFolder","WorkProduct"));
		request.setQueryFilter(new QueryFilter("Tester", "=", userId).and(new QueryFilter("CreationDate", ">=", "2017-08-25")).and(new QueryFilter("CreationDate", "<=", "2017-09-25")));
		RallyRestApi restApi = new RallyRestApi(new URI("https://rally1.rallydev.com/"), RALLY_KEY);
		QueryResponse response = restApi.query(request);
		if (response.getResults().size() > 0) {
			System.out.println("TOTAL RECORDS : " + response.getResults().size());

			for (int i = 0; i < response.getResults().size(); i++) {
				System.out.println("NUMBER : " + response.getResults().get(i).getAsJsonObject().get("TestCase").getAsJsonObject().get("FormattedID").getAsString());
				System.out.println("CreationDate : " + response.getResults().get(i).getAsJsonObject().get("CreationDate").getAsString());
				System.out.println("WorkProduct : " + response.getResults().get(i).getAsJsonObject().get("TestCase").getAsJsonObject().get("WorkProduct").isJsonNull());
				
				
			}
		}
	}
	
	public static void listTask() throws IOException, URISyntaxException {

		String userId = "e068411@mastercard.com";
		QueryRequest request = new QueryRequest(RallyEntity.TASK.requestType);
		request.setFetch(new Fetch("FormattedID", "CreationDate", "_refObjectName", "State","Estimate","Date"));
		request.setQueryFilter(new QueryFilter("Tester", "=", userId).and(new QueryFilter("CreationDate", ">=", "2017-08-25")).and(new QueryFilter("CreationDate", "<=", "2017-09-25")));
		RallyRestApi restApi = new RallyRestApi(new URI("https://rally1.rallydev.com/"), RALLY_KEY);
		QueryResponse response = restApi.query(request);
		if (response.getResults().size() > 0) {
			System.out.println("TOTAL RECORDS : " + response.getResults().size());

			for (int i = 0; i < response.getResults().size(); i++) {
				System.out.println("NUMBER : " + response.getResults().get(i).getAsJsonObject().get("TestCase").getAsJsonObject().get("FormattedID").getAsString());
				System.out.println("CreationDate : " + response.getResults().get(i).getAsJsonObject().get("CreationDate").getAsString());
			}
		}
	}

	@Test
	public void testJson(){

		String json = "{\n" +
				"    \"id\": 22,\n" +
				"    \"products\": [\n" +
				"        {\n" +
				"            \"id\": 1000,\n" +
				"            \"name\": \"FONE C/MIC LOGITECH G233 981-000702 PRODIGY PRE\",\n" +
				"            \"description\": \"FONE C/MIC LOGITECH G233 981-000702 PRODIGY PRE\",\n" +
				"            \"cost\": 42.50\n" +
				"        },\n" +
				"        {\n" +
				"            \"id\": 2000,\n" +
				"            \"name\": \"TENIS C/ LUCES LED FASHION C/RODAS N.33 LILAS/ROSA\",\n" +
				"            \"description\": \"TENIS C/ LUCES LED FASHION C/RODAS N.33 LILAS/ROSA\",\n" +
				"            \"cost\": 9.80\n" +
				"        }\n" +
				"    ],\n" +
				"    \"_links\": {\n" +
				"        \"self\": {\n" +
				"            \"href\": \"https://cart-service-di20026792.cfapps.io/carts/22\"\n" +
				"        },\n" +
				"        \"cart\": {\n" +
				"            \"href\": \"https://cart-service-di20026792.cfapps.io/carts/22\"\n" +
				"        }\n" +
				"    }\n" +
				"}";


		Assert.assertTrue(json.contains("\"id\": 22"));


	}

	public enum RallyEntity {
		TEST_FOLDER("Test Folder", "testfolder"),
		TEST_CASE("Test Case", "testcase"),
		TEST_SET("Test Set","testset"),
		STORY("Story","hierarchicalrequirement"),
		CONVERSATION_POST("Conversation Post", "conversationpost"),
		REVISION("Revision", "revision"),
		TEST_CASE_RESULT("Test Case Result","testcaseresult"),
		TASK("Task","task");

		final String name;
		final String requestType;

		RallyEntity(String name, String requestType) {
			this.name = name;
			this.requestType = requestType;
		}
	}

}

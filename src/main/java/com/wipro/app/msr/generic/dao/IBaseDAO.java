package com.wipro.app.msr.generic.dao;

import java.io.Serializable;
import java.util.List;

import com.wipro.app.msr.util.exceptions.DAOException;

public interface IBaseDAO<T, P> extends Serializable{

	public static final String INSERT = ".insert";
	public static final String UPDATE = ".update";
	public static final String DELETE = ".delete";
	public static final String SELECT_BY_ID = ".select-by-id";
	public static final String SELECT_ALL = ".select-all";
	public static final String SELECT_BY = ".select-by";
	
	public void openSession();
	
	public void closeSession();

	public T selectById(String entity, P id)  throws DAOException;

	public List<T> selectAll(String entity) throws DAOException;

	public List<T> selectBy(String entity, T object) throws DAOException;

	public List<T> selectList(String queryId) throws DAOException;

	public List<T> selectList(String queryId, Object parameter) throws DAOException;
	
	public T selectObject(String queryId, Object parameter) throws DAOException;
		
	public void insert(String entity, T objeto) throws DAOException;
	
	public void insert(String entity, T objeto, String dateFormat) throws DAOException;

	public void update(String entity, T objeto) throws DAOException;
	
	public void update(String entity, T objeto, String dateFormat) throws DAOException;

	public void delete(String entity, P id) throws DAOException;

	public void insertObject(String queryId, Object parameter) throws DAOException; 

	public void updateObject(String queryId, Object parameter) throws DAOException;

	public void deleteObject(String queryId, Object parameter) throws DAOException;
}
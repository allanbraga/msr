package com.wipro.app.msr.util.formatter;

import org.apache.ibatis.type.EnumTypeHandler;

import com.wipro.app.msr.util.constants.RoleEnum;

public class RoleEnumFormatter extends EnumTypeHandler<RoleEnum>{
	
	public RoleEnumFormatter() {
		super(RoleEnum.class);
	}
	
}
package com.wipro.app.msr.controller.story;

import java.util.Date;
import java.util.Objects;

import com.wipro.app.msr.controller.businessleader.BusinessLeader;
import com.wipro.app.msr.controller.resource.Resource;
import com.wipro.app.msr.project.Project;
import com.wipro.app.msr.sow.Sow;
import com.wipro.app.msr.util.constants.RoleEnum;
import com.wipro.app.msr.util.constants.TicketStatusEnum;
import com.wipro.app.msr.util.interfaces.IPrimaryKey;

public class Story implements IPrimaryKey<Integer> {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String number;
	private Integer points;
	private Date date;
	private String comments;
	private RoleEnum role;
	private Resource resource;
	private BusinessLeader businessLeader;
	private TicketStatusEnum status;
	private Project project;
	private Sow sow;
	
	//Filters
	private String startDate;
	private String endDate;

	public Story() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public RoleEnum getRole() {
		return role;
	}

	public void setRole(RoleEnum role) {
		this.role = role;
	}

	public TicketStatusEnum getStatus() {
		return status;
	}

	public void setStatus(TicketStatusEnum status) {
		this.status = status;
	}

	public BusinessLeader getBusinessLeader() {
		return businessLeader;
	}

	public void setBusinessLeader(BusinessLeader businessLeader) {
		this.businessLeader = businessLeader;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public Sow getSow() {
		return sow;
	}

	public void setSow(Sow sow) {
		this.sow = sow;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 53 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Story other = (Story) obj;
		return Objects.equals(this.id, other.id);
	}

	@Override
	public String toString() {
		return "Story [id=" + id + ", number=" + number + ", points=" + points + ", date=" + date + ", comments="
				+ comments + ", role=" + role + ", resource=" + resource + ", businessLeader=" + businessLeader
				+ ", status=" + status + "]";
	}

	@Override
	public Integer getPrimaryKey() {
		return id;
	}

}
